SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create   function [dbo].[trim](@Text nvarchar(max))
returns nvarchar(max)
as  
begin  
    return LTRIM(RTRIM(@Text))
end 
GO
