SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vc_BRF_CollateralTypes] AS SELECT distinct CollateralTypeID, Description FROM dbo.t_CollateralTypes;     -- CollateralsTypes
GO
