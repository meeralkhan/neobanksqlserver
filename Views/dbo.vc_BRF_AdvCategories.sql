SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vc_BRF_AdvCategories] AS SELECT CategoryID, Description FROM t_AdvCategories WITH (NOLOCK) ;     -- Adv Category Sector
GO
