SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vc_BRF_Customer_IR] AS SELECT * FROM t_Customer WITH (NOLOCK)  WHERE CategoryId = 'Individual' AND ResidentNonResident = 'Y';  -- Indi resident
GO
