SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[v_SavingsAccruals]
AS
select * from t_RptSavAccruals where AccrualDate IN (select max(AccrualDate) from t_RptSavAccruals)
GO
