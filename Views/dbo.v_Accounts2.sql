SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[v_Accounts2] AS SELECT COUNT([Account ID]) AS [No Of Accounts], SUM([Drawable Balance]) AS [Total Balance] FROM v_Accounts
GO
