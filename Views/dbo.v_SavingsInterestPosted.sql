SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[v_SavingsInterestPosted]
AS
select OurBranchID, AccountID, AccountName, ProductID, CurrencyID, PayDate, FromDate,ToDate, NetProfit AS PayAmount, CreateBy, CreateTime, CreateTerminal from t_RptSavProfit2
GO
