SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vc_BRF_Customer_C] AS SELECT * FROM t_Customer WITH (NOLOCK)  WHERE CategoryId <> 'Individual' ;  -- Corp
GO
