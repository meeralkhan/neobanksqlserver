SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[v_DepositsInterestPosted]
as
select * from t_ProfitPayHistory
where [Date] IN (select MAX([Date]) from t_ProfitPayHistory)
GO
