SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[vc_AccountLimit]  
AS  
select OurBranchID, ClientID, FacilityID, SerialID, AccountID, AccountLimitID, AuthStatus, Limit, EffectiveDate, Limit1, Rate1, Limit2, Rate2, Limit3, Rate3, ExcessRate  
from t_LinkFacilityAccount  
GO
