SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[v_OverdraftAccruals]
as
select * from t_RptODAccruals
where AccrualDate IN (select MAX(AccrualDate) from t_RptODAccruals)
GO
