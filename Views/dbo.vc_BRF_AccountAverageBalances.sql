SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[vc_BRF_AccountAverageBalances] AS SELECT * FROM t_AccountAverageBalances WITH (NOLOCK) ;     -- AccountAverageBalances
GO
