SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[v_RolloverEncashTDR]
as
select * from t_TDRRollover
where wDate IN (select MAX(wDate) from t_TDRRollover)
GO
