SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[v_GetAccountMonthBalances]
AS
select * from t_AccountMonthBalances
where [Month] IN (select MAX([Month]) from t_AccountMonthBalances)
GO
