SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[v_GetAutoSweeps]
as
select * from t_AccountSweepsLogs
where wDate IN (select max(wDate) from t_AccountSweepsLogs)
GO
