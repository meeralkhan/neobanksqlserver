SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[vc_GLBalanceHistory]
As
select * from t_GLBalanceHistory where wDate = (select max(wDate) AS wDate from t_GLBalanceHistory)
GO
