SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[v_DepositsAccruals]
as
select * from t_AccrualHistory
where [Date] IN (select MAX([Date]) from t_AccrualHistory)
GO
