SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vc_BRF_Industry] AS SELECT Distinct IndustryType, Term FROM t_Industry WITH (NOLOCK) ;     -- Adv Category Sector
GO
