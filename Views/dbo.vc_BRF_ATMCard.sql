SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vc_BRF_ATMCard] AS SELECT * FROM dbo.t_DebitCards  WITH (NOLOCK) where Status = 'A';     -- Active Debit Cards
GO
