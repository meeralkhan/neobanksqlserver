SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[rptUserLogs]  
AS  
-- rptUserLogs  
select distinct OurBranchID, OperatorID, WDateTime,Description, Terminal from t_UserAccessLogs  
where Description <> 'Home'

GO
