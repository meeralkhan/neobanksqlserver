SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[vc_BRF_ATMTransactions] AS SELECT * FROM t_Transactions WITH (NOLOCK) WHERE DocumentType LIKE 'AT%';     -- ATM Transactions
GO
