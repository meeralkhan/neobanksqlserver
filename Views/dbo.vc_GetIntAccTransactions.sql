SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   view [dbo].[vc_GetIntAccTransactions]
AS
select t.OurBranchID, t.ScrollNo, '1' SerialNo, t.RefNo, t.AccountID, t.CurrencyID, t.ProductID, t.AccountType,
t.ValueDate, t.wDate, t.DescriptionID, t.Amount, t.TrxType, t.ForeignAmount, t.ExchangeRate, t.IsLocalCurrency, t.TrxTimeStamp
from t_Transactions t
where t.AccountType = 'C' and t.[Status] <> 'R'
GO
