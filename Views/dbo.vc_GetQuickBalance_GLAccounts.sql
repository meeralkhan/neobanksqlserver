SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [dbo].[vc_GetQuickBalance_GLAccounts]
AS
select OurBranchID, AccountID, Description, 'GL' AS ProductID, CurrencyID, case CurrencyID when 'AED' then 0 else ForeignBalance end AS FCYBalance, Balance AS LCYBalance
from t_GL where AccountClass = 'P'
GO
