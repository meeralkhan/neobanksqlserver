CREATE TABLE [dbo].[t_BBTransactions]
(
[ScrollNo] [int] NOT NULL,
[SerialNo] [int] NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValueDate] [datetime] NOT NULL,
[wDate] [datetime] NOT NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[ForeignAmount] [money] NOT NULL,
[ExchangeRate] [decimal] (18, 6) NOT NULL,
[DescriptionID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsLocalCurrency] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceBranch] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TargetBranch] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Remarks] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
