CREATE TABLE [dbo].[t_ZXAutoRenewelLogs]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1),
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ZXAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[DateTime] [datetime] NOT NULL,
[Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScrollNo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JsonData] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_ZXAutoRenewelLogs] ADD CONSTRAINT [PK_t_ZXAutoRenewelLogs] PRIMARY KEY CLUSTERED ([SerialNo], [OurBranchID], [AccountID], [wDate])
GO
