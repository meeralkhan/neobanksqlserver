CREATE TABLE [dbo].[t_DDMandate]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [decimal] (24, 0) NOT NULL,
[Purpose] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Status] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReferenceNo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NULL,
[AmountType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MinAmount] [money] NULL,
[MaxAmount] [money] NULL
)
GO
ALTER TABLE [dbo].[t_DDMandate] ADD CONSTRAINT [PK_t_DDMandate] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [SerialNo])
GO
