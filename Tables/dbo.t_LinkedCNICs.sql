CREATE TABLE [dbo].[t_LinkedCNICs]
(
[SerialNo] [numeric] (18, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CNICNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OTP] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsUsed] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[t_LinkedCNICs] ADD CONSTRAINT [PK_t_LinkedCNICs] PRIMARY KEY CLUSTERED ([SerialNo])
GO
