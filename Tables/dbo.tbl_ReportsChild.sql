CREATE TABLE [dbo].[tbl_ReportsChild]
(
[ReportId] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FkTable] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Alias] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JoinType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[JoinCondition] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[tbl_ReportsChild] ADD CONSTRAINT [PK_tbl_ReportsChild] PRIMARY KEY CLUSTERED ([ReportId], [FkTable])
GO
