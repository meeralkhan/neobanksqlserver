CREATE TABLE [dbo].[t_CBSData]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TerminalName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Transaction_Particular] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValueDate] [datetime] NOT NULL,
[TrxDate] [datetime] NOT NULL,
[TrxIdentifier] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TerminalID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Customer_Account_Date] [datetime] NOT NULL,
[TrxTime] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STAN] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthIDResp] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RetRefNum] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAN] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[Balance] [money] NOT NULL,
[CardAccptID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
