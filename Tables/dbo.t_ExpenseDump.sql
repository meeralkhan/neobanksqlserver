CREATE TABLE [dbo].[t_ExpenseDump]
(
[wDate] [datetime] NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CostCenterID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL
)
GO
ALTER TABLE [dbo].[t_ExpenseDump] ADD CONSTRAINT [PK_t_ExpenseDump] PRIMARY KEY CLUSTERED ([wDate], [OurBranchID], [AccountID], [CostCenterID])
GO
