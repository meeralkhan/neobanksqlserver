CREATE TABLE [dbo].[t_LoanRepaymentHistory]
(
[CreateBy] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Createtime] [datetime] NOT NULL,
[updateby] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[updatetime] [datetime] NULL,
[DealRefNo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FacilityId] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealId] [decimal] (18, 0) NOT NULL,
[Clientid] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RePaymentType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WDate] [datetime] NOT NULL,
[SerialNo] [decimal] (18, 0) NOT NULL,
[Amount] [money] NOT NULL,
[ScrollNo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchid] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsClosed] [bit] NULL,
[CloseDate] [datetime] NULL,
[ForceDebit] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransactionDate] [datetime] NOT NULL,
[PaymentMode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PaymentSource] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_LoanRepaymentHistory] ADD CONSTRAINT [PK_t_LoanRepaymentHistory] PRIMARY KEY CLUSTERED ([DealRefNo], [FacilityId], [DealId], [SerialNo], [OurBranchid])
GO
