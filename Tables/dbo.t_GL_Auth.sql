CREATE TABLE [dbo].[t_GL_Auth]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrintID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoryID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountClass] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPosting] [int] NULL,
[IsContigent] [int] NULL,
[OpeningBalance] [money] NULL,
[Balance] [money] NULL,
[ForeignOpeningBalance] [money] NULL,
[ForeignBalance] [money] NULL,
[TemporaryBalance] [money] NULL,
[Thru] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShadowBalance] [money] NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AvgBalProcessing] [int] NULL,
[RevaluationProcess] [int] NULL,
[IsReconcile] [int] NULL,
[GLOwnerID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLFSRollUps] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLSubGroups] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsMetaDataIncluded] [int] NULL,
[isExpenseMgmt] [int] NULL,
[CostTypesSPOID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_GL_Auth] ADD CONSTRAINT [PK_t_GL_Auth] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID])
GO
