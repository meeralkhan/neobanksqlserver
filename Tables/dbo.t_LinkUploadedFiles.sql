CREATE TABLE [dbo].[t_LinkUploadedFiles]
(
[UploadID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FrmName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKID1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKID2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PKID3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PKID4] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UploadPath] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_LinkUploadedFiles] ADD CONSTRAINT [PK_t_LinkUploadedFiles] PRIMARY KEY CLUSTERED ([UploadID])
GO
