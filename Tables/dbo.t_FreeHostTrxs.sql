CREATE TABLE [dbo].[t_FreeHostTrxs]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1),
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STAN] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHXDate] [datetime] NULL,
[wDate] [datetime] NULL,
[Amount] [numeric] (18, 6) NULL,
[ForeignAmount] [numeric] (18, 6) NULL,
[ExchangeRate] [decimal] (16, 8) NULL,
[ConvRate] [decimal] (15, 9) NULL,
[MerchantType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcqCountryCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForwardInstID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ATMID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrCodeTran] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrCodeSett] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlmntAmount] [numeric] (18, 6) NULL,
[ProcCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSEntryMode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSConditionCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSPINCaptCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcqInstID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RetRefNum] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardAccptID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardAccptNameLoc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VISATrxID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CAVV] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DescriptionID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoOfCWTrxATMMonthly] [decimal] (24, 0) NULL,
[NoOfBITrxATMMonthly] [decimal] (24, 0) NULL
)
GO
ALTER TABLE [dbo].[t_FreeHostTrxs] ADD CONSTRAINT [PK__t_FreeHo__5E5A535F1FB8F68E] PRIMARY KEY CLUSTERED ([SerialNo])
GO
