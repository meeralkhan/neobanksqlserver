CREATE TABLE [dbo].[t_OL_TransactionStatus]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScrollNo] [numeric] (18, 0) NULL,
[SerialNo] [int] NULL,
[RefNo] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [money] NULL,
[ForeignAmount] [money] NULL,
[ExchangeRate] [money] NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supervised] [bit] NULL,
[TrxType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsLocalCurrency] [bit] NULL,
[TransactionMode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
