CREATE TABLE [dbo].[t_DealMonthBalances]
(
[Month] [datetime] NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [decimal] (24, 0) NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearBalance] [money] NULL,
[Effects] [money] NULL,
[LocalClearBalance] [money] NULL,
[LocalEffects] [money] NULL
)
GO
ALTER TABLE [dbo].[t_DealMonthBalances] ADD CONSTRAINT [PK_t_DealMonthBalances] PRIMARY KEY CLUSTERED ([Month], [OurBranchID], [AccountID], [DealID])
GO
