CREATE TABLE [dbo].[t_ATM_ErrorDescription]
(
[ErrorSerial] [numeric] (18, 0) NOT NULL IDENTITY(1, 1),
[ErrorDate] [datetime] NOT NULL,
[ErrorTime] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ErrorDescription] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
