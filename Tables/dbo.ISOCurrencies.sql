CREATE TABLE [dbo].[ISOCurrencies]
(
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyDescription] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyAlphaCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ISOCurrencies] ADD CONSTRAINT [PK_ISOCurrencies] PRIMARY KEY CLUSTERED ([CurrencyCode])
GO
