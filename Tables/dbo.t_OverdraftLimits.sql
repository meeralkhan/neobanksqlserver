CREATE TABLE [dbo].[t_OverdraftLimits]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FacilityID] [decimal] (24, 0) NOT NULL,
[SerialID] [decimal] (24, 0) NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountLimitID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Limit] [money] NOT NULL,
[EffectiveDate] [datetime] NOT NULL,
[Limit1] [money] NOT NULL,
[Rate1] [decimal] (18, 6) NOT NULL,
[MinRate1] [decimal] (18, 6) NOT NULL,
[Limit2] [money] NOT NULL,
[Rate2] [decimal] (18, 6) NOT NULL,
[MinRate2] [decimal] (18, 6) NOT NULL,
[Limit3] [money] NOT NULL,
[Rate3] [decimal] (18, 6) NOT NULL,
[MinRate3] [decimal] (18, 6) NOT NULL,
[ExcessRate] [decimal] (18, 6) NOT NULL,
[ExcessMinRate] [decimal] (18, 6) NOT NULL,
[LogDateTime] [datetime] NOT NULL,
[NextChangeDate] [datetime] NULL
)
GO
ALTER TABLE [dbo].[t_OverdraftLimits] ADD CONSTRAINT [PK_t_OverdraftLimits] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [EffectiveDate])
GO
