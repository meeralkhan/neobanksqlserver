CREATE TABLE [dbo].[aCostCenterExpenses]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CCPercent] [decimal] (38, 3) NULL
)
GO
