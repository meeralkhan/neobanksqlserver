CREATE TABLE [dbo].[Quantum_BRF_10]
(
[DEAL_NO] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEAL_DT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MATURE_DT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPARTYNAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BOR_INVEST] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FACE_VALUE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SWIFT_FI] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ToolRunDate] [datetime] NULL
)
GO
