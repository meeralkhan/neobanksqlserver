CREATE TABLE [dbo].[t_Clearing]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScrollNo] [int] NULL,
[SerialNo] [int] NULL,
[ValueDate] [datetime] NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChequeDate] [datetime] NULL,
[BankID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[ForeignAmount] [money] NOT NULL,
[TrxID] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsLocalCurrency] [int] NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsForceClear] [int] NOT NULL,
[ClearBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearTime] [datetime] NULL,
[ClearTerminal] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_Clearing] ADD CONSTRAINT [PK_t_Clearing] PRIMARY KEY CLUSTERED ([OurBranchID], [RefNo])
GO
