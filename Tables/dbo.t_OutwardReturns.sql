CREATE TABLE [dbo].[t_OutwardReturns]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScrollNo] [int] NOT NULL,
[SerialNo] [int] NOT NULL,
[BankID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchID] [char] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeDate] [datetime] NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NULL,
[ValueDate] [datetime] NULL,
[Amount] [money] NOT NULL,
[ForeignAmount] [money] NULL,
[ExchangeRate] [money] NULL,
[Reason] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Charges] [money] NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_OutwardReturns] ADD CONSTRAINT [PK_t_OutwardReturns] PRIMARY KEY CLUSTERED ([OurBranchID], [RefNo])
GO
