CREATE TABLE [dbo].[t_TDRLienUnLien]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecieptID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [decimal] (24, 0) NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsLien] [int] NOT NULL,
[LienAmount] [decimal] (21, 3) NOT NULL,
[LienAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LienRemarks] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LienBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LienTime] [datetime] NOT NULL,
[LienTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnLienRemarks] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnLienBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnLienTime] [datetime] NULL,
[UnLienTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_TDRLienUnLien] ADD CONSTRAINT [PK_t_TDRLienUnLien] PRIMARY KEY CLUSTERED ([OurBranchID], [RecieptID], [SerialNo])
GO
