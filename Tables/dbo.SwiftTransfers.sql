CREATE TABLE [dbo].[SwiftTransfers]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[ChannelRefId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InwardOutward] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxPurpose] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AmountInAED] [numeric] (25, 4) NULL,
[DateOfTrx] [datetime] NULL,
[SQLPostedDate] [datetime] NULL,
[Txn_Id] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToAccountNumber] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SwiftTransfers] ADD CONSTRAINT [PK_SwiftTransfers] PRIMARY KEY CLUSTERED ([Id])
GO
