CREATE TABLE [dbo].[t_SMSTable]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[wDate] [datetime] NULL,
[AccountType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChequeID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChequeDate] [datetime] NULL,
[Amount] [decimal] (21, 3) NULL,
[ForeignAmount] [decimal] (21, 3) NULL,
[ExchangeRate] [decimal] (18, 6) NULL,
[DescriptionID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChannelRefID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxTimeStamp] [datetime] NULL,
[SMSText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SMSToSend] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Id] [int] NOT NULL IDENTITY(1, 1),
[IsSend] [bit] NULL,
[RequestParameter] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseParameter] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL
)
GO
ALTER TABLE [dbo].[t_SMSTable] ADD CONSTRAINT [PK_t_SMSTable] PRIMARY KEY CLUSTERED ([Id])
GO
