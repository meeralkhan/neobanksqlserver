CREATE TABLE [dbo].[t_ClientBorrowingMandate_Auth]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialID] [decimal] (24, 0) NOT NULL,
[MinLimit] [money] NULL,
[MaxLimit] [money] NULL,
[AuthGroupID1] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthGroupID2] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthGroupID3] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthGroupID4] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MandateType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthGroupID5] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthGroupID6] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_ClientBorrowingMandate_Auth] ADD CONSTRAINT [PK_t_ClientBorrowingMandate_Auth] PRIMARY KEY CLUSTERED ([OurBranchID], [ClientID], [SerialID])
GO
