CREATE TABLE [dbo].[t_GLBalanceHistory]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ForeignBalance] [money] NULL,
[Balance] [money] NOT NULL
)
GO
ALTER TABLE [dbo].[t_GLBalanceHistory] ADD CONSTRAINT [PK_t_GLBalanceHistory] PRIMARY KEY CLUSTERED ([OurBranchID], [wDate], [AccountID])
GO
