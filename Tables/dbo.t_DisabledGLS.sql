CREATE TABLE [dbo].[t_DisabledGLS]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[AccountClass] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLOwnerID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OldIsPosting] [int] NULL,
[IsPosting] [int] NOT NULL,
[LogID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[t_DisabledGLS] ADD CONSTRAINT [PK_tbl_DisabledGLS] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [wDate])
GO
