CREATE TABLE [dbo].[t_CashTransactionModel]
(
[ScrollNo] [numeric] (10, 0) NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValueDate] [datetime] NULL,
[wDate] [datetime] NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChequeDate] [datetime] NULL,
[Amount] [money] NOT NULL,
[ForeignAmount] [money] NULL,
[ExchangeRate] [decimal] (18, 6) NULL,
[DescriptionID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxPrinted] [bit] NOT NULL,
[ProfitOrLoss] [money] NOT NULL,
[GlID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supervision] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemoteDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSupervision] [bit] NOT NULL,
[IsLocalCurrency] [int] NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DebitPostingLimit] [money] NULL,
[CreditPostingLimit] [money] NULL,
[AppWHTax] [bit] NOT NULL,
[WHTaxAmount] [money] NOT NULL,
[WHTaxMode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WHTaxScrollNo] [int] NOT NULL,
[UtilityNumber] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppChg] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChgAmount] [money] NULL,
[ChgMode] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChgScrollno] [money] NULL,
[AdditionalData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[tr_CashTrxCashDr] ON [dbo].[t_CashTransactionModel]    
FOR INSERT    
AS    
    
 IF (Select TRXTYPE from inserted)='D' and (Select ACCOUNTTYPE from Inserted)='C'    
 AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions where ISNULL(IsInternal,1) = 0 and IsCredit = 0)    
    
 Update t_Account SET CashTotDr=ISNULL(CashTotDr,0)+(SELECT Abs(AMOUNT) FROM INSERTED),    
  CashTotDrF=ISNULL(CashTotDrF,0)+(SELECT Abs(ForeignAmount) FROM INSERTED)    
    
 WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=(SELECT AccountID FROM INSERTED)     
 AND  (select ACCOUNTTYPE from inserted)='C'  And (select TRXTYPE from inserted)='D'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[tr_CashTrxCashReject] ON [dbo].[t_CashTransactionModel]   
  
FOR  UPDATE   
AS  
  
 If ((Select supervision from inserted)='R' AND  (select ACCOUNTTYPE from inserted)='C'  
  AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions   
  where ISNULL(IsInternal,1) = 0 and IsCredit = 0) And (select TRXTYPE from inserted)='D')  
  
 Update t_Account SET CashTotDr=CashTotDr-(SELECT abs(AMOUNT) FROM INSERTED),  
  CashTotDrF=CashTotDrF-(SELECT abs(ForeignAmount) FROM INSERTED)  
  
 WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=(SELECT AccountID FROM INSERTED)   
 AND  (select ACCOUNTTYPE from inserted)='C'  And (select TRXTYPE from inserted)='D'
GO
ALTER TABLE [dbo].[t_CashTransactionModel] ADD CONSTRAINT [PK_t_CashTransactionModel] PRIMARY KEY CLUSTERED ([ScrollNo], [OurBranchID])
GO
