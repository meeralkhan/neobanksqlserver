CREATE TABLE [dbo].[t_MismatchGLLogs]
(
[SerialNo] [int] NOT NULL IDENTITY(1, 1),
[OurBranchId] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InvalidAccountId] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxDateTime] [datetime] NOT NULL,
[WorkingDate] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[t_MismatchGLLogs] ADD CONSTRAINT [PK_t_MismatchGLLogs] PRIMARY KEY CLUSTERED ([SerialNo])
GO
