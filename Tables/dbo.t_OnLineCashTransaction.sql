CREATE TABLE [dbo].[t_OnLineCashTransaction]
(
[ScrollNo] [int] NOT NULL,
[SerialNo] [int] NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValueDate] [datetime] NOT NULL,
[wDate] [datetime] NOT NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeDate] [datetime] NOT NULL,
[Amount] [money] NOT NULL,
[ForeignAmount] [money] NOT NULL,
[ExchangeRate] [decimal] (18, 6) NOT NULL,
[DescriptionID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Supervision] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsLocalCurrency] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceBranch] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TargetBranch] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Remarks] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdditionalData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppWHTax] [smallint] NOT NULL,
[WHTaxAmount] [money] NOT NULL,
[WHTaxMode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WHTScrollNo] [int] NOT NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[tr_OnLineCashTransaction] ON [dbo].[t_OnLineCashTransaction]     
FOR INSERT    
AS     
    
 DECLARE @Counter int    
 DECLARE @ClearBalance money    
 DECLARE @LocalClearBalance money    
-- DECLARE @TransferScroll  As Numeric(10,0)    
-- DECLARE @TransferSerial  As Numeric(10,0)    
 DECLARE @ScrollNo   As Numeric(10,0)    
 DECLARE @SerialNo   As Numeric(10,0)    
 DECLARE @OurBranchID  As varchar(4)    
 DECLARE @AccountID   As varchar(15)    
 DECLARE @AccountName  As char(50)    
 DECLARE @ProductID   As varchar(6)    
 Declare @AccountType   As char(1)    
 DECLARE @CurrencyID  As varchar(4)    
 DECLARE @ValueDate   As datetime    
 DECLARE @wDate   As datetime    
 DECLARE @TrxType   As char(1)    
 DECLARE @ChequeID     As varchar(11)    
 DECLARE @ChequeDate   As datetime    
 DECLARE @Amount   As money    
 DECLARE @ForeignAmount  As money    
 DECLARE @ExchangeRate As numeric(18,6)    
 DECLARE @DescriptionID As varchar(4)    
 DECLARE @Description   As varchar(255)    
 Declare @OperatorID  As varchar(15)    
 Declare @SupervisorID   As varchar(15)    
 DECLARE @Supervision As char(1)    
 DECLARE @IsLocalCurrency As bit    
 DECLARE @SourceBranch As varchar(4)    
 DECLARE @TargetBranch As varchar(4)    
 DECLARE @RefNo   As varchar(20)    
 DECLARE @BankID   As varchar(4)    
     
 SELECT  @ScrollNo  =  ScrollNo,    
   @SerialNo  =  SerialNo,    
   @OurBranchID = OurBranchID,     
   @AccountID  = AccountID,      
   @AccountName  =  AccountName,    
   @ProductID  = ProductID,     
   @CurrencyID  =  CurrencyID,    
   @AccountType  =   AccountType,    
   @ValueDate   =  ValueDate,      
   @wDate   =  wDate,    
   @TrxType   =  TrxType,    
   @ChequeID     =   ChequeID,    
   @ChequeDate   = ChequeDate,     
   @Amount   = Amount,     
   @ForeignAmount = ForeignAmount,    
   @ExchangeRate = ExchangeRate,     
   @DescriptionID = DescriptionID,     
   @Description   = Description,     
   @Supervision = Supervision,    
   @IsLocalCurrency=  IsLocalCurrency,    
   @OperatorID   =  OperatorID,     
   @SupervisorID  =  SupervisorID ,    
   @SourceBranch =  SourceBranch,     
   @TargetBranch = TargetBranch,    
   @RefNo   = ScrollNo    
   
 FROM  Inserted    
    
    
 IF (@AccountType ='C') AND  (@Supervision = 'C')    
 BEGIN       
      
  SELECT @Counter = Count(*) FROM t_AccountBalance    
   WHERE OurBranchID = @OurBranchID AND AccountID= @AccountID    
      
  IF @Counter <= 0     
   BEGIN    
    RAISERROR ('Account not found, In Account Balance...' ,16, 1)     
   END    
  ELSE    
  BEGIN    
      
   IF @TrxType = 'C' AND @IsLocalCurrency = '1'     
   BEGIN    
        
    SELECT @ClearBalance=ClearBalance, @LocalClearBalance=LocalClearBalance     
                FROM t_AccountBalance WHERE OurBranchID= @OurBranchID AND AccountID  = @AccountID     
                    
    UPDATE t_AccountBalance SET    
    ClearBalance = @ClearBalance + @Amount,    
    LocalClearBalance = @LocalClearBalance + @Amount    
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID     
      
    /* Cash Total Cr - Local Starts */  
      
    IF @DescriptionID IN (select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID AND ISNULL(IsInternal,1) = 0 and IsCredit = 1)  
    begin  
       Update t_Account SET CashTotCr = ISNULL(CashTotCr,0)+Abs(@Amount)  
       WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
    end  
      
    /* Cash Total Cr - Local Ends */  
      
   END    
   ELSE IF @TrxType = 'C'      
   BEGIN    
        
    SELECT @ClearBalance=ClearBalance, @LocalClearBalance=LocalClearBalance     
                FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID    
                    
    UPDATE t_AccountBalance SET    
    ClearBalance=@ClearBalance + @ForeignAmount,    
    LocalClearBalance=@LocalClearBalance + @Amount      
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
      
    /* Cash Total Cr - Foreign Starts */  
      
    IF @DescriptionID IN (select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID AND ISNULL(IsInternal,1) = 0 and IsCredit = 1)  
    begin  
       Update t_Account SET CashTotCr = ISNULL(CashTotCr,0)+Abs(@Amount), CashTotCrF = ISNULL(CashTotCrF,0)+Abs(@ForeignAmount)  
       WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
    end  
      
    /* Cash Total Cr - Foreign Ends */  
      
   END    
   ELSE IF @TrxType = 'D' AND @IsLocalCurrency = '1'     
   BEGIN    
        
    SELECT @ClearBalance=ClearBalance, @LocalClearBalance=LocalClearBalance    
    FROM t_AccountBalance WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID     
        
    UPDATE t_AccountBalance SET    
    ClearBalance=@ClearBalance - @Amount,    
    LocalClearBalance=@LocalClearBalance - @Amount      
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID     
      
    /* Cash Total Dr - Local Starts */  
      
    IF @DescriptionID IN (select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID AND ISNULL(IsInternal,1) = 0 and IsCredit = 0)  
    begin  
       Update t_Account SET CashTotDr = ISNULL(CashTotDr,0)+Abs(@Amount)  
       WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
    end  
      
    /* Cash Total Dr - Local Ends */  
      
   END    
   ELSE IF @TrxType = 'D'    
   BEGIN    
        
    SELECT @ClearBalance=ClearBalance, @LocalClearBalance=LocalClearBalance    
    FROM t_AccountBalance WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID     
        
    UPDATE t_AccountBalance SET    
    ClearBalance=@ClearBalance - @ForeignAmount ,    
    LocalClearBalance=@LocalClearBalance - @Amount    
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID  
      
    /* Cash Total Dr - Foreign Starts */  
      
    IF @DescriptionID IN (select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID AND ISNULL(IsInternal,1) = 0 and IsCredit = 0)  
    begin  
       Update t_Account SET CashTotDr = ISNULL(CashTotDr,0)+Abs(@Amount), CashTotDrF = ISNULL(CashTotDrF,0)+Abs(@ForeignAmount)  
       WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
    end  
      
    /* Cash Total Dr - Foreign Ends */  
      
   END    
       
   --Get Last ScrollNo    
/*   SELECT @TransferScroll = ScrollNo, @TransferSerial = SerialNo     
                                  FROM t_TransferTransactionModel    
    WHERE RefNo = @RefNo    
   IF @@RowCount > 0     
    BEGIN     
    Select @ScrollNo = @TransferScroll    
    Select @SerialNo = @TransferSerial + 1    
    END    
   ELSE    
    BEGIN     
    SELECT @ScrollNo = IsNull(Max(ScrollNo),0) + 1     
      FROM t_TransferTransactionModel    
    Select @SerialNo = 1    
    END    
      
   --Get BankId    
   Select @BankID = IsNull(BankId,'') From t_Branches     
                                 Where BranchID = @SourceBranch    
    
   INSERT INTO t_TransferTransactionModel    
           (ScrollNo, SerialNo, Refno, OurBranchID, AccountID, AccountName, ProductID, CurrencyID, AccountType,     
           ValueDate, wDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, DescriptionID,     
           Description, BankCode, BranchCode, TrxPrinted, ProfitOrLoss, GLID, Supervision, RemoteDescription,     
    IsSupervision, IsLocalCurrency, OperatorID, SupervisorID)     
   VALUES (@ScrollNo, @SerialNo, @RefNo, @OurBranchID, @AccountID, @AccountName, @ProductID, @CurrencyID,     
    @AccountType, @ValueDate, @wDate, @TrxType, @ChequeID, @ChequeDate, @Amount, @ForeignAmount,     
    @ExchangeRate, @DescriptionID, @Description, @BankID, @SourceBranch, 0, 0, ' ', @Supervision, ' ',    
           0, @IsLocalCurrency, @OperatorID, @SupervisorID)    
*/    
       
   --IF @TrxType = 'D'  AND UPPER(@ChequeID) <>'V'    
   --BEGIN    
   -- Insert Into t_ChequePaid(OurBranchID,  AccountID, ChequeID, Date, OperatorID, SupervisorID, AccountType)    
   -- values (@OurBranchID,@AccountID, @ChequeID,@ValueDate,@OperatorID,@SupervisorID,@AccountType)    
   --END   
      END    
 END
GO
ALTER TABLE [dbo].[t_OnLineCashTransaction] ADD CONSTRAINT [PK_t_OnLineCashTransaction_1] PRIMARY KEY CLUSTERED ([ScrollNo], [SerialNo], [OurBranchID])
GO
