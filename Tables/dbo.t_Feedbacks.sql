CREATE TABLE [dbo].[t_Feedbacks]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FeedbackID] [decimal] (24, 0) NOT NULL,
[SerialNo] [decimal] (24, 0) NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Adjustment] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Required] [int] NULL,
[ComboItems] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderId] [int] NULL,
[RangeTo] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[t_Feedbacks] ADD CONSTRAINT [PK_t_Feedbacks] PRIMARY KEY CLUSTERED ([OurBranchID], [FeedbackID], [SerialNo])
GO
