CREATE TABLE [dbo].[tbl_Form_Grids]
(
[FrmName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GridId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSelect] [int] NOT NULL,
[TabId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PortletId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SectionId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tbl_Form_Grids] ADD CONSTRAINT [PK_tbl_Form_Grids] PRIMARY KEY CLUSTERED ([FrmName], [GridId])
GO
