CREATE TABLE [dbo].[t_WalletTokens]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[Token] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[ExpireTime] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[t_WalletTokens] ADD CONSTRAINT [PK_t_WalletTokens] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID])
GO
