CREATE TABLE [dbo].[SchedulerTest]
(
[LogID] [nvarchar] (450) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTime] [datetime] NULL
)
GO
ALTER TABLE [dbo].[SchedulerTest] ADD CONSTRAINT [PK_SchedulerTest] PRIMARY KEY CLUSTERED ([LogID])
GO
