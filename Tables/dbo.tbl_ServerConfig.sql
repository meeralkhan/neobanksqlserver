CREATE TABLE [dbo].[tbl_ServerConfig]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[PrimaryServerIP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[tbl_ServerConfig] ADD CONSTRAINT [PK_tbl_ServerConfig] PRIMARY KEY CLUSTERED ([ID])
GO
