CREATE TABLE [dbo].[ISOCountries]
(
[CountryCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CountryName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ISOCountries] ADD CONSTRAINT [PK_ISOCountries] PRIMARY KEY CLUSTERED ([CountryCode])
GO
