CREATE TABLE [dbo].[ClosedGLs]
(
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ClosedGLs] ADD CONSTRAINT [PK_ClosedGLs] PRIMARY KEY CLUSTERED ([AccountID])
GO
