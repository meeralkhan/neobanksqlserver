CREATE TABLE [dbo].[t_PrevGLBalances]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrintID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoryID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountClass] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPosting] [int] NULL,
[IsContigent] [int] NULL,
[OpeningBalance] [money] NULL,
[Balance] [money] NULL,
[ForeignOpeningBalance] [money] NULL,
[ForeignBalance] [money] NULL,
[TemporaryBalance] [money] NULL,
[Thru] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShadowBalance] [money] NULL,
[AvgBalProcessing] [int] NULL,
[RevaluationProcess] [int] NULL,
[IsReconcile] [int] NULL
)
GO
ALTER TABLE [dbo].[t_PrevGLBalances] ADD CONSTRAINT [PK_t_PrevGLBalances] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID])
GO
