CREATE TABLE [dbo].[t_Cashback321]
(
[SerialID] [decimal] (24, 0) NOT NULL IDENTITY(1, 1),
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[ForeignAmount] [decimal] (21, 3) NOT NULL,
[ExchangeRate] [decimal] (18, 6) NOT NULL,
[Amount] [decimal] (21, 3) NOT NULL,
[CashbackPercent] [money] NOT NULL,
[CashbackAmount] [money] NOT NULL,
[DescriptionID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChannelRefID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxTimeStamp] [datetime] NOT NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[IsPaid] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[t_Cashback321] ADD CONSTRAINT [PK_t_Cashback321] PRIMARY KEY CLUSTERED ([SerialID])
GO
