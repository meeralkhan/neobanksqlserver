CREATE TABLE [dbo].[t_EmailTable]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[AccountId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsEmailSend] [bit] NOT NULL,
[CreatedOn] [datetime] NULL,
[EmailSendDate] [datetime] NULL,
[Subject] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChannelRefId] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestParameter] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseParameter] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[attachmentBase64] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[attachmentBaseFileName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_EmailTable] ADD CONSTRAINT [PK_t_EmailTable] PRIMARY KEY CLUSTERED ([Id])
GO
