CREATE TABLE [dbo].[t_TLInterestRepricingLogs]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[DealID] [decimal] (24, 0) NOT NULL,
[ReferenceNo] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RequestParams] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseParams] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_TLInterestRepricingLogs] ADD CONSTRAINT [PK_t_TLInterestRepricingLogs] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [wDate], [DealID])
GO
