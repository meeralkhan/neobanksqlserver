CREATE TABLE [dbo].[t_AccountCharges]
(
[OurBranchId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReceiptID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeSerial] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NULL,
[ChargeonPrincipal] [int] NOT NULL,
[ChargeonInterest] [int] NOT NULL,
[Type] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsApplicable] [int] NOT NULL,
[ModeOfCharges] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsRoundingRequired] [int] NOT NULL,
[RoundingPrecision] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_AccountCharges] ADD CONSTRAINT [PK_t_AccountCharges] PRIMARY KEY CLUSTERED ([OurBranchId], [ReceiptID], [ChargeId], [ChargeSerial])
GO
