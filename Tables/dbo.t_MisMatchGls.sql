CREATE TABLE [dbo].[t_MisMatchGls]
(
[OurBranchId] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyId] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_MisMatchGls] ADD CONSTRAINT [PK_t_MisMatchGls] PRIMARY KEY CLUSTERED ([OurBranchId], [CurrencyId], [AccountId])
GO
