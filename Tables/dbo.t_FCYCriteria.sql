CREATE TABLE [dbo].[t_FCYCriteria]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CriteriaID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Branch] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Currencies] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SameCurrencies] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DNarrationIDs] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CNarrationIDs] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LookupAccountType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsEnabled] [int] NULL,
[Source] [int] NULL,
[SourceBranchID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceAccntSeries] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Target] [int] NULL,
[TargetBranchID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetAccntSeries] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceCurrency] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetCurrency] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_FCYCriteria] ADD CONSTRAINT [PK_t_FCYCriteria] PRIMARY KEY CLUSTERED ([OurBranchID], [CriteriaID])
GO
