CREATE TABLE [dbo].[t_HO_Account]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseTime] [datetime] NOT NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastRecWDate] [datetime] NULL,
[RealTimeComment] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShortageComment] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcessComment] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CloseAccountTrxRefNo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_HO_Account] ADD CONSTRAINT [PK_t_HO_Account] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID])
GO
