CREATE TABLE [dbo].[t_WaiveInterest]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [decimal] (24, 0) NOT NULL,
[SerialNo] [decimal] (24, 0) NOT NULL,
[ReliefType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Percent] [money] NULL,
[Amount] [money] NULL,
[wDate] [datetime] NULL,
[Remarks] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScrollNo] [decimal] (24, 0) NOT NULL,
[OutInterest] [money] NOT NULL,
[OutPInterest] [money] NOT NULL,
[Type] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PackageID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_WaiveInterest] ADD CONSTRAINT [PK_t_WaiveInterest] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [DealID], [SerialNo])
GO
