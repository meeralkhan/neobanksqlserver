CREATE TABLE [dbo].[t_CustCorporateKYC]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OpenDate] [datetime] NULL,
[NameCustLocal] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationCustLocal] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressCustLocal] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NameCustCountryWide] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationCustCountryWide] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressCustCountryWide] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NameCustInt] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationCustInt] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressCustInt] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RiskAssessment] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CRAFormID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpiryDate] [datetime] NULL,
[KYCStatus] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_CustCorporateKYC] ADD CONSTRAINT [PK_t_CustCorporateKYC] PRIMARY KEY CLUSTERED ([OurBranchID], [ClientID], [SerialNo])
GO
