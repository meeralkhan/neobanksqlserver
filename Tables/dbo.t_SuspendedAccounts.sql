CREATE TABLE [dbo].[t_SuspendedAccounts]
(
[wDate] [datetime] NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [decimal] (24, 0) NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RiskCode] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Accrual] [money] NOT NULL,
[PAccrual] [money] NOT NULL,
[GLInterestReceived] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLSuspendedInterest] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPosted] [int] NOT NULL,
[IsReversed] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[t_SuspendedAccounts] ADD CONSTRAINT [PK_t_SuspendedAccounts] PRIMARY KEY CLUSTERED ([wDate], [OurBranchID], [AccountID], [DealID])
GO
