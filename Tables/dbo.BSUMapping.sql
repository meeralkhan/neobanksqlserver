CREATE TABLE [dbo].[BSUMapping]
(
[GUID] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperatorID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastActivityTime] [datetime] NOT NULL,
[BSUData] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[BSUMapping] ADD CONSTRAINT [PK_BSUMapping] PRIMARY KEY CLUSTERED ([OperatorID], [OurBranchID])
GO
