CREATE TABLE [dbo].[t_CustomerSicCodeCorp]
(
[OurBranchId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SicCodeCorp] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_CustomerSicCodeCorp] ADD CONSTRAINT [PK_t_CustomerSicCodeCorp] PRIMARY KEY CLUSTERED ([OurBranchId], [ClientID], [SicCodeCorp])
GO
