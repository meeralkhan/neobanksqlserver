CREATE TABLE [dbo].[t_ATM_Message]
(
[Message] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MessageDate] [datetime] NOT NULL,
[Remarks] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_ATM_Message] ADD CONSTRAINT [PK_t_ATM_Message] PRIMARY KEY CLUSTERED ([MessageDate])
GO
