CREATE TABLE [dbo].[t_RecSaveODAccounts]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeanRate] [decimal] (18, 6) NULL,
[CcyRounding] [decimal] (24, 0) NULL,
[IBAN] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpenDate] [datetime] NULL,
[Accrual] [money] NULL,
[AccrualUpto] [datetime] NULL,
[tAccrual] [money] NOT NULL,
[IsProcessed] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[t_RecSaveODAccounts] ADD CONSTRAINT [PK_t_RecSaveODAccounts] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID])
GO
