CREATE TABLE [dbo].[t_InternetIBCANo]
(
[IBCA] [int] NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TargetBranch] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IBDA] [int] NOT NULL
)
GO
