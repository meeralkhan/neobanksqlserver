CREATE TABLE [dbo].[t_AccountSweeps]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [decimal] (24, 0) NOT NULL,
[ReferenceID] [nvarchar] (450) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Action] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SIAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [money] NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SIAccountID2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargesTrxRef] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_AccountSweeps] ADD CONSTRAINT [PK_t_AccountSweeps] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [SerialNo])
GO
