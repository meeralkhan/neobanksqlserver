CREATE TABLE [dbo].[t_ChannelRefIDs]
(
[ChannelRefID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_ChannelRefIDs] ADD CONSTRAINT [PK_t_ChannelRefID] PRIMARY KEY CLUSTERED ([ChannelRefID])
GO
