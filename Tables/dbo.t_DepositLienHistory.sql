CREATE TABLE [dbo].[t_DepositLienHistory]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReceiptID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialID] [decimal] (18, 0) NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LienDate] [datetime] NOT NULL,
[LienRemarks] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsLienMarkInAccount] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LienMarkAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LienMarkAccountName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LienMarkOperatorID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnLienDate] [datetime] NULL,
[UnLienRemarks] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnLienOperatorID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
