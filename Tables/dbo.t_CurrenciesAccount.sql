CREATE TABLE [dbo].[t_CurrenciesAccount]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CashSerialID] [int] NOT NULL,
[HOSerialID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[t_CurrenciesAccount] ADD CONSTRAINT [PK_t_CurrenciesAccount] PRIMARY KEY CLUSTERED ([OurBranchID], [CurrencyID])
GO
