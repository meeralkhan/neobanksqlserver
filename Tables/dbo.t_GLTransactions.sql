CREATE TABLE [dbo].[t_GLTransactions]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VoucherID] [numeric] (10, 0) NOT NULL,
[SerialID] [numeric] (10, 0) NOT NULL,
[Date] [datetime] NOT NULL,
[ValueDate] [datetime] NULL,
[DescriptionID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[ForeignAmount] [money] NOT NULL,
[ExchangeRate] [decimal] (16, 8) NULL,
[IsCredit] [bit] NOT NULL,
[TransactionType] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransactionMethod] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Indicator] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScrollNo] [int] NOT NULL,
[SerialNo] [int] NOT NULL,
[AdditionalData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsMainTrx] [smallint] NOT NULL,
[DocType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GlID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IDRefNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceBranch] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TargetBranch] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClearingType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RegisterDate] [datetime] NULL,
[Remarks] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MerchantType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreditCardNumber] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[STAN] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CompanyNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChannelId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VirtualAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VirtualAccountTitle] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChannelRefID] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConvRate] [decimal] (15, 9) NULL,
[CurrCodeTran] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcqCountryCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrCodeSett] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlmntAmount] [numeric] (18, 6) NULL,
[POSEntryMode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSConditionCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSPINCaptCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcqInstID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RetRefNum] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthIDResp] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardAccptID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardAccptNameLoc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VISATrxID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxTimeStamp] [datetime] NOT NULL,
[TrxDesc] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MCC] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CAVV] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForwardInstID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrevDayEntry] [int] NULL,
[VatID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Percent] [money] NULL,
[RejectRemarks] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrevYearEntry] [int] NULL,
[TClientId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAccountid] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TProductId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostCenterID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsYearEndProcess] [int] NULL,
[GLVendorID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[memo2] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostTypeID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerLifecycleID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectId] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostExpenseID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VatReferenceID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_GLTransactionDelete] ON [dbo].[t_GLTransactions]         
FOR DELETE        
as        
 declare @LocalCurrency varchar(4)        
 select @localCurrency=LocalCurrency From t_GlobalVariables        
   
 declare @PrevDayEntry int    
 select @PrevDayEntry = ISNULL(PrevDayEntry,0) from deleted    
  
 if (select TransactionMethod from deleted ) = 'L'        
 BEGIN        
 if (select Indicator from deleted ) = '*'        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
 else        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.Balance = t_gl.Balance -  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.Balance = t_gl.Balance +  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
        
 END        
 else        
 if (select TransactionMethod from  deleted ) = 'F'        
 BEGIN        
 if (select Indicator from deleted ) = '*'        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select ForeignAmount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select ForeignAmount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select ForeignAmount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select ForeignAmount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
 else        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ForeignBalance = t_gl.ForeignBalance -  (select ForeignAmount from deleted ),        
  t_gl.Balance = t_gl.Balance -  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.ForeignBalance -  (select ForeignAmount from deleted ),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ForeignBalance = t_gl.foreignBalance +  (select ForeignAmount from deleted ),        
  t_gl.Balance = t_gl.Balance -  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.foreignBalance +  (select ForeignAmount from deleted ),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from deleted )  
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
        
 END        
 else        
 if (select TransactionMethod from deleted )='A'        
 BEGIN        
 if (select currencyid from deleted ) <> @LocalCurrency        
 BEGIN        
 if (select Indicator from deleted ) = '*'        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select ForeignAmount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select ForeignAmount  from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select ForeignAmount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select ForeignAmount  from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
 END        
 END        
 else        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ForeignBalance = t_gl.ForeignBalance -  (select ForeignAmount  from deleted ),        
  t_gl.Balance = t_gl.Balance -  (select Amount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.ForeignBalance -  (select ForeignAmount  from deleted ),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount  from deleted )  
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ForeignBalance = t_gl.foreignBalance +  (select ForeignAmount  from deleted ),        
  t_gl.Balance = t_gl.Balance +  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.foreignBalance +  (select ForeignAmount  from deleted ),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from deleted )  
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
 END        
 else        
 BEGIN        
 if (select Indicator from deleted ) = '*'        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select Amount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select Amount  from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select Amount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select Amount  from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
 else        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
/*       SET t_gl.ForeignBalance = t_gl.ForeignBalance -  (select Amount from deleted ),*/        
  set t_gl.Balance = t_gl.Balance -  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       set t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
/*       SET t_gl.ForeignBalance = t_gl.foreignBalance +  (select Amount from deleted ),*/        
  SET t_gl.Balance = t_gl.Balance +  (select Amount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount  from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
 END        
           
END 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_GLTransactionIns] ON [dbo].[t_GLTransactions]       
FOR INSERT      
as      
 declare @LocalCurrency varchar(4)      
 select @localCurrency=LocalCurrency From t_GlobalVariables  
  
 declare @PrevDayEntry int  
 select @PrevDayEntry = ISNULL(PrevDayEntry,0) from inserted  
  
 if (select TransactionMethod from inserted ) = 'L'      
 BEGIN      
 if (select Indicator from inserted ) = '*'      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select Amount from Inserted)      
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select Amount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
 else      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.Balance = t_gl.Balance +  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from Inserted)  
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.Balance = t_gl.Balance -  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
      
 END      
 else      
 if (select TransactionMethod from inserted ) = 'F'      
 BEGIN      
 if (select Indicator from inserted ) = '*'      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select ForeignAmount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select ForeignAmount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select ForeignAmount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select ForeignAmount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
 else      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ForeignBalance = t_gl.ForeignBalance +  (select ForeignAmount from Inserted),      
  t_gl.Balance = t_gl.Balance +  (select Amount from Inserted)       
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.ForeignBalance +  (select ForeignAmount from Inserted),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from Inserted)       
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ForeignBalance = t_gl.foreignBalance -  (select ForeignAmount from Inserted),      
  t_gl.Balance = t_gl.Balance +  (select Amount from Inserted)       
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.foreignBalance -  (select ForeignAmount from Inserted),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from Inserted)       
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
      
 END      
 else      
 if (select TransactionMethod from inserted)='A'      
 BEGIN      
 if (select currencyid from inserted) <> @LocalCurrency      
 BEGIN      
 if (select Indicator from inserted ) = '*'      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select ForeignAmount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select ForeignAmount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select ForeignAmount  from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select ForeignAmount  from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
 else      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ForeignBalance = t_gl.ForeignBalance +  (select ForeignAmount from Inserted),      
  t_gl.Balance = t_gl.Balance +  (select Amount  from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.ForeignBalance +  (select ForeignAmount from Inserted),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount  from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ForeignBalance = t_gl.foreignBalance -  (select ForeignAmount from Inserted),      
  t_gl.Balance = t_gl.Balance -  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.foreignBalance -  (select ForeignAmount from Inserted),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
      
 END      
 else      
 BEGIN      
 if (select Indicator from inserted ) = '*'      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select Amount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select Amount  from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select Amount  from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
 else      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.Balance = t_gl.Balance +  (select Amount from Inserted)             
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from Inserted)             
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.Balance = t_gl.Balance -  (select Amount  from Inserted)             
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount  from Inserted)             
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
      
 END      
END    
GO
