CREATE TABLE [dbo].[Quantum_BRF_FX]
(
[DEAL_NO] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEAL_DT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MATURE_DT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPARTYNAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTYPECODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BUYCCY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SELLCCY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BASE_CCY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BASE_RATE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INT_RATE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BUYAMOUNT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SELLAMOUNT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANS_TYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECNAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IN_USE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRYCODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCCODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCNAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ToolRunDate] [datetime] NULL
)
GO
