CREATE TABLE [dbo].[t_TransferTransactionModel]
(
[ScrollNo] [int] NOT NULL,
[SerialNo] [numeric] (10, 0) NOT NULL,
[RefNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [char] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValueDate] [datetime] NULL,
[wDate] [datetime] NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChequeDate] [datetime] NULL,
[Amount] [numeric] (18, 6) NOT NULL,
[ForeignAmount] [numeric] (18, 6) NULL,
[ExchangeRate] [decimal] (18, 6) NULL,
[DescriptionID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxPrinted] [bit] NOT NULL,
[ProfitOrLoss] [money] NOT NULL,
[GlID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Supervision] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemoteDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSupervision] [bit] NOT NULL,
[IsLocalCurrency] [int] NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AdditionalData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AppWHTax] [bit] NOT NULL,
[WHTaxScrollNo] [int] NOT NULL,
[ChannelId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VirtualAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VirtualAccountTitle] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChannelRefID] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxTimeStamp] [datetime] NOT NULL,
[PrevDayEntry] [int] NULL,
[ChargeID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VatID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Percent] [money] NULL,
[RejectRemarks] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrevYearEntry] [int] NULL,
[TClientId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAccountid] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TProductId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostCenterID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLVendorID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[memo2] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VendorID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostTypeID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CustomerLifecycleID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProjectId] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostExpenseID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VatReferenceID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[tr_TransferTrxCashDr] ON [dbo].[t_TransferTransactionModel]    
FOR INSERT    
AS    
   
 declare @TaxFiler int  
   
 select @TaxFiler = ISNULL(TaxFiler,0) FROM t_Account  
 WHERE OurBranchID = (SELECT OurBranchID FROM INSERTED)  
 AND AccountID = (SELECT AccountID FROM INSERTED)  
   
 IF @TaxFiler = 0  
 BEGIN  
      
    IF (Select TRXTYPE from inserted)='D' and (Select ACCOUNTTYPE from Inserted)='C'    
    AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions  
    where ISNULL(IsInternal,1) = 0 and IsCredit = 0)  
      
    Update t_Account SET CashTotDr=ISNULL(CashTotDr,0)+(SELECT Abs(AMOUNT) FROM INSERTED),  
    CashTotDrF=ISNULL(CashTotDrF,0)+(SELECT Abs(ForeignAmount) FROM INSERTED)  
    WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=(SELECT AccountID FROM INSERTED)  
    AND (select ACCOUNTTYPE from inserted)='C'  And (select TRXTYPE from inserted)='D'  
      
 END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[tr_TransferTrxCashReject] ON [dbo].[t_TransferTransactionModel]   
  
FOR  UPDATE   
AS  
 declare @TaxFiler int  
   
 select @TaxFiler = ISNULL(TaxFiler,0) FROM t_Account  
 WHERE OurBranchID = (SELECT OurBranchID FROM INSERTED)  
 AND AccountID = (SELECT AccountID FROM INSERTED)  
   
 IF @TaxFiler = 0  
 BEGIN  
      
     If ((Select supervision from inserted)='R' AND  (select ACCOUNTTYPE from inserted)='C'  
  AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions   
  where ISNULL(IsInternal,1) = 0 and IsCredit = 0) And (select TRXTYPE from inserted)='D')  
    
  Update t_Account SET CashTotDr=CashTotDr-(SELECT abs(AMOUNT) FROM INSERTED),  
  CashTotDrF=CashTotDrF-(SELECT abs(ForeignAmount) FROM INSERTED)  
  WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=(SELECT AccountID FROM INSERTED)  
  AND (select ACCOUNTTYPE from inserted)='C'  And (select TRXTYPE from inserted)='D'  
    
 END
GO
ALTER TABLE [dbo].[t_TransferTransactionModel] ADD CONSTRAINT [PK_t_TransferTransactionModel] PRIMARY KEY CLUSTERED ([ScrollNo], [SerialNo], [OurBranchID])
GO
