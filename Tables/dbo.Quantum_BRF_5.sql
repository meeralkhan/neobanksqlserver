CREATE TABLE [dbo].[Quantum_BRF_5]
(
[DEAL_NO] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEAL_DT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVTYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BBGID] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FACE_VALUE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRYCODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REMAIN_FV] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RATING] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RATINGTYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MKT_VALUE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ToolRunDate] [datetime] NULL
)
GO
