CREATE TABLE [dbo].[t_LinkCIFCollateral]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [decimal] (24, 0) NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Limit] [money] NULL,
[LimitType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GroupID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReferenceNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FacilityDP] [money] NULL,
[cExpiryDate] [datetime] NULL,
[DateReview] [datetime] NULL,
[DateExpiry] [datetime] NULL,
[CollateralID] [decimal] (24, 0) NULL,
[FacilityDesc] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditProgramID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LimitSecType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status2] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionBy] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionTime] [datetime] NULL,
[ActionTerminal] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupervisedBy] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupervisedTime] [datetime] NULL,
[SupervisedTerminal] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CancelReferenceNo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifyReferenceNo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_LinkCIFCollateral] ADD CONSTRAINT [PK_t_LinkCIFCollateral] PRIMARY KEY CLUSTERED ([OurBranchID], [ClientID], [SerialNo])
GO
