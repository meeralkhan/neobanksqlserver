CREATE TABLE [dbo].[t_RptTLPrincipal]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1),
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InstNo] [decimal] (24, 0) NOT NULL,
[DealRefNo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeanRate] [decimal] (18, 6) NOT NULL,
[wDate] [datetime] NOT NULL,
[PAmount] [decimal] (21, 3) NOT NULL,
[PAmountLq] [decimal] (21, 3) NOT NULL,
[PAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PAccountName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PCurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PMeanRate] [decimal] (18, 6) NOT NULL
)
GO
ALTER TABLE [dbo].[t_RptTLPrincipal] ADD CONSTRAINT [PK_t_RptTLPrincipal] PRIMARY KEY CLUSTERED ([SerialNo])
GO
