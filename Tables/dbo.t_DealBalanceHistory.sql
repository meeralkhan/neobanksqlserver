CREATE TABLE [dbo].[t_DealBalanceHistory]
(
[wDate] [datetime] NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [decimal] (24, 0) NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OutstandingBalance] [money] NOT NULL,
[OutstandingBalanceLCY] [money] NOT NULL,
[Accrual] [money] NOT NULL,
[PAccrual] [money] NOT NULL,
[tAccrual] [money] NOT NULL,
[tPAccrual] [money] NOT NULL,
[RiskCode] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NoOfDPD] [decimal] (24, 0) NOT NULL
)
GO
ALTER TABLE [dbo].[t_DealBalanceHistory] ADD CONSTRAINT [PK_t_DealBalanceHistory] PRIMARY KEY CLUSTERED ([wDate], [OurBranchID], [AccountID], [DealID])
GO
