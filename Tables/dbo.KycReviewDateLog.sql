CREATE TABLE [dbo].[KycReviewDateLog]
(
[Id] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OldKycReviewDate] [datetime] NULL,
[NewKycReviewDate] [datetime] NULL,
[RiskProfile] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BlackList] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PEP] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BlackListCorp] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PEPCorp] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedOn] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[KycReviewDateLog] ADD CONSTRAINT [PK_KycReviewDateLog] PRIMARY KEY CLUSTERED ([Id])
GO
