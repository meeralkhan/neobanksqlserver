CREATE TABLE [dbo].[t_TDRIssuance]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecieptID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAccountType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAccount] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [money] NULL,
[ExchangeRate] [decimal] (18, 6) NULL,
[IssueDate] [datetime] NULL,
[StartDate] [datetime] NULL,
[MaturityDate] [datetime] NULL,
[Rate] [money] NULL,
[AutoProfit] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentPeriod] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Treatment] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoRollover] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OptionsAtMaturity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPremiumRate] [int] NULL,
[AdditionalData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsUnderLien] [int] NULL,
[IsLost] [int] NULL,
[LostRemarks] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Interest] [money] NOT NULL,
[InterestPaid] [money] NOT NULL,
[InterestPaidupTo] [datetime] NULL,
[IsClosed] [int] NULL,
[CloseDate] [datetime] NULL,
[Accrual] [money] NOT NULL,
[AccrualUpto] [datetime] NULL,
[tAccrual] [money] NOT NULL,
[Penalty] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PenaltyRateType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PenaltyAmountRate] [money] NULL,
[TotalPenaltyAmount] [money] NULL,
[PartialAmount] [money] NULL,
[TotalLienAmount] [decimal] (21, 3) NULL,
[OldReceiptID] [decimal] (24, 0) NULL,
[ParentReceiptID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartialDate] [datetime] NULL,
[PartialWithdrawRemarks] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CloseRemarks] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoOfDays] [int] NULL,
[PartialInterestAmount] [money] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_TDRIssuanceHistory]  
ON [dbo].[t_TDRIssuance]  
AFTER  INSERT,UPDATE  
AS  
IF UPDATE(Penalty) OR UPDATE(PenaltyRateType) OR UPDATE(PenaltyAmountRate) OR UPDATE(TotalPenaltyAmount) OR UPDATE(Amount) OR UPDATE(UpdateTime)  
 begin  
 declare @RecieptID nvarchar(30)  
 select @RecieptID = RecieptID from inserted  
  insert into t_TDRIssuanceHistory (CreateBy,CreateTime,CreateTerminal,UpdateBy,UpdateTime,UpdateTerminal,AuthStatus,SuperviseBy,SuperviseTime,SuperviseTerminal,frmName,SerialNo,OurBranchID,RecieptID,ProductID,AccountID,NAccountType,NAccount,NProductID,Amount,ExchangeRate,IssueDate,StartDate,MaturityDate,Rate,AutoProfit,PaymentPeriod,Treatment,AutoRollover,  
  OptionsAtMaturity,IsPremiumRate,AdditionalData,RefNo,IsUnderLien,IsLost,LostRemarks,VerifyBy,VerifyTime,VerifyTerminal,VerifyStatus,Interest,InterestPaid,InterestPaidupTo,IsClosed,CloseDate,Accrual,AccrualUpto  
  ,tAccrual,Penalty,PenaltyRateType,PenaltyAmountRate,TotalPenaltyAmount,PartialAmount,TotalLienAmount,OldReceiptID,ParentReceiptID,PartialDate) select CreateBy,CreateTime,CreateTerminal,UpdateBy,UpdateTime,UpdateTerminal,AuthStatus,SuperviseBy,SuperviseTime,SuperviseTerminal,frmName,(select isnull(max(serialno),0)+1 from   
  t_TDRIssuanceHistory where RecieptID = @RecieptID),OurBranchID,RecieptID,ProductID,AccountID,NAccountType,NAccount,NProductID,Amount,ExchangeRate,IssueDate,StartDate,MaturityDate,Rate,AutoProfit,PaymentPeriod,Treatment,AutoRollover,  
  OptionsAtMaturity,IsPremiumRate,AdditionalData,RefNo,IsUnderLien,IsLost,LostRemarks,VerifyBy,VerifyTime,VerifyTerminal,VerifyStatus,Interest,InterestPaid,InterestPaidupTo,IsClosed,CloseDate,Accrual,AccrualUpto  
  ,tAccrual,Penalty,PenaltyRateType,PenaltyAmountRate,TotalPenaltyAmount,PartialAmount,TotalLienAmount,OldReceiptID,ParentReceiptID,PartialDate from t_TDRIssuance where RecieptID = @RecieptID  
 end  
GO
ALTER TABLE [dbo].[t_TDRIssuance] ADD CONSTRAINT [PK_t_TDRIssuance] PRIMARY KEY CLUSTERED ([OurBranchID], [RecieptID])
GO
