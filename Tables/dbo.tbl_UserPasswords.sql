CREATE TABLE [dbo].[tbl_UserPasswords]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTeminal] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
