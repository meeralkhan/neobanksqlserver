CREATE TABLE [dbo].[t_CostAllocationGL]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CostAllocationGLID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StaffGLID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PremiseGLID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OthersGLID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PremiseOutGLID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherOutGLID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[cmbStaffOutGLID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_CostAllocationGL] ADD CONSTRAINT [PK_t_CostAllocationGL] PRIMARY KEY CLUSTERED ([OurBranchID], [CostAllocationGLID])
GO
