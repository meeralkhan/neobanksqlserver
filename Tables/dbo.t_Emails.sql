CREATE TABLE [dbo].[t_Emails]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[eFrom] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eFromName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eHost] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eSMTPSecurity] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ePass] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eKey] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eTo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eToName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eSubject] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eBody] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eSent] [int] NOT NULL,
[eSentTime] [datetime] NULL,
[eAttempts] [int] NOT NULL,
[eDateTime] [datetime] NOT NULL,
[eTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_Emails] ADD CONSTRAINT [PK_t_Emails] PRIMARY KEY CLUSTERED ([OurBranchID], [SerialNo])
GO
