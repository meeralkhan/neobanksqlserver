CREATE TABLE [dbo].[t_AutoCloseHistory]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClearBalance] [money] NOT NULL,
[BeforMarkStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TerminalID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_AutoCloseHistory] ADD CONSTRAINT [PK_t_AutoCloseHistory] PRIMARY KEY CLUSTERED ([OurBranchID], [wDate], [ProductID], [AccountID])
GO
