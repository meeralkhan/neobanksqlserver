CREATE TABLE [dbo].[t_Currencies]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BuyingRate] [decimal] (18, 6) NULL,
[SellingRate] [decimal] (18, 6) NULL,
[PreferredBuy] [decimal] (18, 6) NULL,
[PreferredSell] [decimal] (18, 6) NULL,
[NotesBuy] [decimal] (18, 6) NULL,
[NotesSell] [decimal] (18, 6) NULL,
[MeanRate] [decimal] (18, 6) NULL,
[CRRounding] [decimal] (24, 0) NULL,
[DBRounding] [decimal] (24, 0) NULL,
[CurrencyCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccCurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RetainedEarningGL] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CBUAEBuy] [decimal] (18, 6) NULL,
[CBUAESell] [decimal] (18, 6) NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
CREATE Trigger [dbo].[Add_CurrencyHistory] ON [dbo].[t_Currencies]    
for insert    
AS          
BEGIN          
SET NOCOUNT ON           
          
  IF EXISTS(SELECT * FROM inserted)          
  BEGIN          
   Declare @WorkingDate datetime, @AuthStatus char(1)          
          
   SELECT @WorkingDate=WorkingDate + ' ' + convert(varchar(10), getdate(), 108) from t_Last           
   WHERE OurBranchID = (Select OurBranchID from inserted)    
           
   select @AuthStatus = AuthStatus from inserted    
                  
   insert into t_Currencies_History (CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime,           
   UpdateTerminal, AuthStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID,           
   ChangeDate, [Description], BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell,           
   MeanRate, CRRounding, DBRounding, CurrencyCode, frmName, AccCurrencyID)          
   select CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, @AuthStatus,           
   SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID, @WorkingDate, [Description],           
   BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell, MeanRate, CRRounding, DBRounding,           
   CurrencyCode, frmName, AccCurrencyID          
   FROM inserted    
   where isnull(AuthStatus,'') = ''          
  END          
END  
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Trigger [dbo].[Delete_CurrencyHistory] ON [dbo].[t_Currencies]        
after delete     
AS        
BEGIN        
 SET NOCOUNT ON         
        
  IF EXISTS(SELECT * FROM deleted)        
  BEGIN        
         
   Declare @WorkingDate datetime        
         
   SELECT @WorkingDate=WorkingDate + ' ' + convert(varchar(10), getdate(), 108) from t_Last       
   WHERE OurBranchID = (Select OurBranchID from deleted)      
                
   -- inserted        
   insert into t_Currencies_History (CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime,         
   UpdateTerminal, AuthStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID,         
   ChangeDate, [Description], BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell,         
   MeanRate, CRRounding, DBRounding, CurrencyCode, frmName, AccCurrencyID)        
   select CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, AuthStatus,         
   SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID, @WorkingDate, [Description],         
   BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell, MeanRate, CRRounding, DBRounding,         
   CurrencyCode, frmName, AccCurrencyID        
   FROM deleted        
  END        
END  
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Trigger [dbo].[Edit_CurrencyHistory] ON [dbo].[t_Currencies]        
for update        
AS        
BEGIN        
SET NOCOUNT ON         
        
  IF EXISTS(SELECT * FROM inserted)        
  BEGIN        
   Declare @WorkingDate datetime, @AuthStatus char(1)        
        
   SELECT @WorkingDate=WorkingDate + ' ' + convert(varchar(10), getdate(), 108) from t_Last         
   WHERE OurBranchID = (Select OurBranchID from Inserted)      
         
   select @AuthStatus = AuthStatus from Inserted        
                
   insert into t_Currencies_History (CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime,         
   UpdateTerminal, AuthStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID,         
   ChangeDate, [Description], BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell,         
   MeanRate, CRRounding, DBRounding, CurrencyCode, frmName, AccCurrencyID)        
   select CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, @AuthStatus,         
   SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID, @WorkingDate, [Description],         
   BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell, MeanRate, CRRounding, DBRounding,         
   CurrencyCode, frmName, AccCurrencyID        
   FROM Inserted  
   where isnull(AuthStatus,'') = ''        
  END        
END
GO
ALTER TABLE [dbo].[t_Currencies] ADD CONSTRAINT [PK_t_Currencies] PRIMARY KEY CLUSTERED ([OurBranchID], [CurrencyID])
GO
