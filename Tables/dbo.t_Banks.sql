CREATE TABLE [dbo].[t_Banks]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FullName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShortName] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MinCommission] [money] NULL,
[CommissionRate] [money] NULL,
[OurCommission] [money] NULL,
[LimitAmount] [money] NULL,
[LocalRemitIssuance] [int] NULL,
[LocalRemitPay] [int] NULL,
[ForeignRemitIssuance] [int] NULL,
[ForeignRemitPay] [int] NULL,
[DDConfirmationAbove] [money] NULL,
[SettlementAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCAcceptance] [int] NULL,
[DocumentsNegotiation] [int] NULL,
[SettlementBankID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlementBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CountryID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SWIFTCode] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SORTCode] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_Banks] ADD CONSTRAINT [PK_t_Banks] PRIMARY KEY CLUSTERED ([OurBranchID], [BankID])
GO
