CREATE TABLE [dbo].[t_TermLoanLimits]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FacilityID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [decimal] (24, 0) NOT NULL,
[DealRefNo] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Limit] [money] NOT NULL,
[EffectiveDate] [datetime] NOT NULL,
[ProfitRateType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProfitRate] [decimal] (18, 6) NOT NULL,
[BaseRate] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BasePercent] [decimal] (18, 6) NULL,
[Margin] [decimal] (18, 6) NULL,
[FloorRate] [decimal] (18, 6) NULL,
[CapRate] [decimal] (18, 6) NULL,
[Limit1] [money] NOT NULL,
[Rate1] [decimal] (18, 6) NOT NULL,
[ExcessRate] [decimal] (18, 6) NOT NULL,
[LogDateTime] [datetime] NOT NULL,
[NextChangeDate] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[t_TermLoanLimits] ADD CONSTRAINT [PK_t_TermLoanLimits] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [DealID], [EffectiveDate])
GO
