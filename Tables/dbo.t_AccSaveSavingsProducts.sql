CREATE TABLE [dbo].[t_AccSaveSavingsProducts]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeanRate] [decimal] (18, 6) NULL,
[CcyRounding] [decimal] (24, 0) NULL,
[CreditInterestStart] [datetime] NULL,
[CreditInterestFrequency] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditRounding] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditInterestDays] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MinCreditInterest] [money] NULL,
[TaxRate] [money] NULL,
[TaxRounding] [nvarchar] (56) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLControl] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLInterestPayable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLInterestPaid] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLTaxAccount] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccrualPostDate] [datetime] NOT NULL,
[AccruedBalance] [money] NOT NULL,
[PassDailyAccrualEntry] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreditInterestProcedure] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsProcessed] [int] NOT NULL,
[Debit321ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_AccSaveSavingsProducts] ADD CONSTRAINT [PK_t_AccSaveSavingsProducts] PRIMARY KEY CLUSTERED ([OurBranchID], [ProductID])
GO
