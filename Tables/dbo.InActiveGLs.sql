CREATE TABLE [dbo].[InActiveGLs]
(
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[InActiveGLs] ADD CONSTRAINT [PK_InActiveGLs] PRIMARY KEY CLUSTERED ([AccountID])
GO
