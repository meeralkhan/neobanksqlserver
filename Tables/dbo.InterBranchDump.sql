CREATE TABLE [dbo].[InterBranchDump]
(
[UniqueID] [nvarchar] (450) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (21, 3) NOT NULL,
[ExchangeRate] [decimal] (18, 6) NOT NULL,
[LocalEq] [decimal] (21, 3) NOT NULL,
[NarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxMethod] [int] NOT NULL,
[SystemDateTime] [datetime] NOT NULL,
[TransitAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MismatchAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChannelRefId] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxMemo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxNarrationId] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[InterBranchDump] ADD CONSTRAINT [PK_InterBranchDump] PRIMARY KEY CLUSTERED ([UniqueID])
GO
