CREATE TABLE [dbo].[LogsArchived]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Application] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Logged] [datetime] NULL,
[Level] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Logger] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Callsite] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Exception] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserId] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeviceID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IP] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FunctionName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ControllerName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartTime] [datetime] NOT NULL,
[EndTime] [datetime] NOT NULL,
[Status] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestParameters] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestResponse] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Channel] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogId] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestDateTime] [datetime] NULL,
[ErrorCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
