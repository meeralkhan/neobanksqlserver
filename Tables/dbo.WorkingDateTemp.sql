CREATE TABLE [dbo].[WorkingDateTemp]
(
[WorkingDateTime] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[WorkingDateTemp] ADD CONSTRAINT [PK_WorkingDateTemp] PRIMARY KEY CLUSTERED ([WorkingDateTime])
GO
