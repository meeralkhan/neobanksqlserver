CREATE TABLE [dbo].[t_AccSaveODProducts]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitInterestProcedure] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitInterestStart] [datetime] NULL,
[DebitRounding] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CcyRounding] [decimal] (24, 0) NULL,
[DebitInterestDays] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PassDailyAccrualEntry] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLInterestReceivable] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLInterestReceived] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinanceProductType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReceivableProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapitalizedProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GlobalRate] [money] NULL,
[GLServiceCharges] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLSuspendedInterest] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvRateBehaviour] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLBadDebt] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLProvisioning] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLProvisionHeld] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccruedBalance] [money] NOT NULL,
[AccrualPostDate] [datetime] NOT NULL,
[IsProcessed] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[t_AccSaveODProducts] ADD CONSTRAINT [PK_t_AccSaveODProducts] PRIMARY KEY CLUSTERED ([OurBranchID], [ProductID])
GO
