CREATE TABLE [dbo].[t_CIF_BeneficialOwners]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BeneficialOwnersID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_CIF_BeneficialOwners] ADD CONSTRAINT [PK_t_CIF_BeneficialOwners] PRIMARY KEY CLUSTERED ([OurBranchID], [ClientID], [BeneficialOwnersID])
GO
