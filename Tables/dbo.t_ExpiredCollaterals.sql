CREATE TABLE [dbo].[t_ExpiredCollaterals]
(
[SerialID] [decimal] (24, 0) NOT NULL IDENTITY(1, 1),
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FacilityID] [decimal] (24, 0) NOT NULL,
[CollateralID] [decimal] (24, 0) NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReviewDate] [datetime] NOT NULL,
[ExpiryDate] [datetime] NOT NULL,
[CollateralValue] [money] NOT NULL,
[AfterDeductingMargin] [money] NOT NULL,
[ExpiredWDate] [datetime] NOT NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_ExpiredCollaterals] ADD CONSTRAINT [PK_t_ExpiredCollaterals] PRIMARY KEY CLUSTERED ([SerialID])
GO
