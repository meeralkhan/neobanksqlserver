CREATE TABLE [dbo].[t_ErrorHandling]
(
[ErrorCode] [decimal] (24, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ErrorName] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_ErrorHandling] ADD CONSTRAINT [PK_t_ErrorHandling] PRIMARY KEY CLUSTERED ([ErrorCode])
GO
