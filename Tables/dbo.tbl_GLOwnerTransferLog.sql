CREATE TABLE [dbo].[tbl_GLOwnerTransferLog]
(
[AccountID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FromGLOwnerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ToGLOwnerID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferDate] [datetime] NULL,
[AuthorizeDate] [datetime] NULL,
[LogID] [int] NULL
)
GO
