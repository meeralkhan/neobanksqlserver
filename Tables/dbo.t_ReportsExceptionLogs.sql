CREATE TABLE [dbo].[t_ReportsExceptionLogs]
(
[SerialId] [int] NOT NULL IDENTITY(1, 1),
[ErrorStartTime] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Exception] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InnerException] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestParams] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[level] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorEndTime] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_ReportsExceptionLogs] ADD CONSTRAINT [PK__t_Report__5E5B3EE4E48D3112] PRIMARY KEY CLUSTERED ([SerialId])
GO
