CREATE TABLE [dbo].[t_PreviousDayBalances_GL]
(
[CreateBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WorkingDate] [datetime] NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountClass] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OpeningBalance] [money] NULL,
[Balance] [money] NULL,
[ForeignOpeningBalance] [money] NULL,
[ForeignBalance] [money] NULL,
[TemporaryBalance] [money] NULL,
[ShadowBalance] [money] NULL
)
GO
ALTER TABLE [dbo].[t_PreviousDayBalances_GL] ADD CONSTRAINT [PK_t_PreviousDayBalances_GL] PRIMARY KEY CLUSTERED ([OurBranchID], [WorkingDate], [AccountID])
GO
