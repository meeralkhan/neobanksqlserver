CREATE TABLE [dbo].[t_InwardClearing]
(
[SerialNo] [int] NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchID] [char] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeDate] [datetime] NOT NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NULL,
[ValueDate] [datetime] NULL,
[DescriptionID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Payee] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [money] NOT NULL,
[ForeignAmount] [money] NULL,
[ExchangeRate] [decimal] (18, 6) NULL,
[CheckedBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reason] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsApplyCharges] [bit] NOT NULL,
[ChargesScrollNo] [int] NOT NULL,
[ChargesAmount] [money] NOT NULL,
[AdditionalData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AppWHTax] [int] NOT NULL,
[WHTaxAmount] [money] NOT NULL,
[WHTaxMode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WHTScrollNo] [int] NOT NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[tr_InwardTrxCashDr] ON [dbo].[t_InwardClearing]    
FOR INSERT    
AS    
    
	IF (Select ACCOUNTTYPE from Inserted)='C'    
	AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions where ISNULL(IsInternal,1) = 0 and IsCredit = 0)    
    
	Update t_Account SET CashTotDr=ISNULL(CashTotDr,0)+(SELECT Abs(AMOUNT) FROM INSERTED),    
	CashTotDrF=ISNULL(CashTotDrF,0)+(SELECT Abs(ForeignAmount) FROM INSERTED)  
   
	WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=(SELECT AccountID FROM INSERTED) AND (select ACCOUNTTYPE from inserted)='C'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 CREATE TRIGGER [dbo].[tr_InwardTrxCashDr_Delete] ON [dbo].[t_InwardClearing]  
FOR DELETE  
AS    
    
	IF (Select ACCOUNTTYPE from Deleted)='C'    
	AND (Select DescriptionID from Deleted) IN (select DescriptionID from t_TransactionDescriptions  
	where ISNULL(IsInternal,1) = 0 and IsCredit = 0)    
    
	Update t_Account SET CashTotDr=ISNULL(CashTotDr,0)-(SELECT Abs(AMOUNT) FROM Deleted),    
	CashTotDrF=ISNULL(CashTotDrF,0)-(SELECT Abs(ForeignAmount) FROM Deleted)  
   
	WHERE OurBranchID=(SELECT OurBranchID FROM Deleted) AND AccountID=(SELECT AccountID FROM Deleted)  
	AND (select ACCOUNTTYPE from Deleted)='C'
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[tr_InwardTrxCashReject] ON [dbo].[t_InwardClearing]   
  
FOR  UPDATE   
AS  
  
	If ((Select [Status] from inserted)='R' AND  (select ACCOUNTTYPE from inserted)='C'  
	AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions   
	where ISNULL(IsInternal,1) = 0 and IsCredit = 0))  
  
	Update t_Account SET CashTotDr=CashTotDr-(SELECT abs(AMOUNT) FROM INSERTED),  
	CashTotDrF=CashTotDrF-(SELECT abs(ForeignAmount) FROM INSERTED)  
  
	WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=  
	(SELECT AccountID FROM INSERTED) AND (select ACCOUNTTYPE from inserted)='C'
GO
ALTER TABLE [dbo].[t_InwardClearing] ADD CONSTRAINT [PK_t_InwardClearing] PRIMARY KEY CLUSTERED ([SerialNo], [OurBranchID])
GO
