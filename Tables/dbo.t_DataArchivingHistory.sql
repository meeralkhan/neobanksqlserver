CREATE TABLE [dbo].[t_DataArchivingHistory]
(
[ParentTableName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Date] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[t_DataArchivingHistory] ADD CONSTRAINT [PK_t_DataArchivingHistory] PRIMARY KEY CLUSTERED ([ParentTableName], [TableName])
GO
