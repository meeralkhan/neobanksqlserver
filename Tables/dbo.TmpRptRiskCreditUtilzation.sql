CREATE TABLE [dbo].[TmpRptRiskCreditUtilzation]
(
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PeakUtilization12M] [money] NULL,
[AvgDailyUtilization12M] [money] NULL,
[PeakUtilizationYTD] [money] NULL,
[AvgDailyUtilizationYTD] [money] NULL,
[RptTime] [datetime] NULL
)
GO
