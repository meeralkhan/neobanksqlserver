CREATE TABLE [dbo].[t_CostAllocationException]
(
[wDate] [datetime] NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CostCenterID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NULL
)
GO
ALTER TABLE [dbo].[t_CostAllocationException] ADD CONSTRAINT [PK_t_CostAllocationException] PRIMARY KEY CLUSTERED ([wDate], [OurBranchID], [AccountID], [CostCenterID])
GO
