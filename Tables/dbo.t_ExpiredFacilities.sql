CREATE TABLE [dbo].[t_ExpiredFacilities]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1),
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FacilityID] [decimal] (24, 0) NOT NULL,
[ReferenceNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FacilityDesc] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReviewDate] [datetime] NOT NULL,
[ExpiryDate] [datetime] NOT NULL,
[Limit] [money] NOT NULL,
[ExpiredWDate] [datetime] NOT NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_ExpiredFacilities] ADD CONSTRAINT [PK_t_ExpiredFacilities] PRIMARY KEY CLUSTERED ([SerialNo])
GO
