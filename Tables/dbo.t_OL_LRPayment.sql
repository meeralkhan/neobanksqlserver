CREATE TABLE [dbo].[t_OL_LRPayment]
(
[ScrollNo] [int] NOT NULL,
[DocumentType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IssueBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InstrumentType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IssueDate] [datetime] NOT NULL,
[InstrumentNo] [int] NOT NULL,
[ControlNo] [int] NOT NULL,
[Amount] [money] NOT NULL,
[ForeignAmount] [money] NOT NULL,
[Beneficiary] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DrawanBank] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DrawanBranch] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_OL_LRPayment] ADD CONSTRAINT [PK_t_OL_LRPayment] PRIMARY KEY CLUSTERED ([ScrollNo], [DocumentType], [IssueBranchID], [wDate])
GO
