CREATE TABLE [dbo].[StoredProcedureExecutionLog]
(
[LogId] [bigint] NOT NULL IDENTITY(1, 1),
[ReportName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportParameter1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogDate] [datetime] NULL
)
GO
ALTER TABLE [dbo].[StoredProcedureExecutionLog] ADD CONSTRAINT [PK_AutoExecutionLog] PRIMARY KEY CLUSTERED ([LogId])
GO
