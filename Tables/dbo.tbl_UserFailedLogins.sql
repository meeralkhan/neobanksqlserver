CREATE TABLE [dbo].[tbl_UserFailedLogins]
(
[BranchId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Username] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Count] [int] NOT NULL,
[UserTime] [datetime] NOT NULL,
[UserTerminal] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[tbl_UserFailedLogins] ADD CONSTRAINT [PK_tbl_UserFailedLogins] PRIMARY KEY CLUSTERED ([BranchId], [Username])
GO
