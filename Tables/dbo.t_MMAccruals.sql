CREATE TABLE [dbo].[t_MMAccruals]
(
[DealNo] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefID] [smallint] NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SecurityID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FromDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[Days] [smallint] NOT NULL
)
GO
