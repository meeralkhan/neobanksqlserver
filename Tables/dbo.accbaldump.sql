CREATE TABLE [dbo].[accbaldump]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearBalance] [money] NULL,
[TrxBalance] [money] NULL,
[BalDiff] [money] NULL
)
GO
