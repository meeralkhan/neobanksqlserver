CREATE TABLE [dbo].[t_ATM_ATMAccount]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ATMID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ATMAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ATMAccountTitle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ATMCurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ATMCurrencyName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ATMProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CDMAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CDMAccountTitle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CDMCurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CDMCurrencyName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CDMProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_ATM_ATMAccount] ADD CONSTRAINT [PK_t_ATM_GLParameters] PRIMARY KEY CLUSTERED ([OurBranchID], [ATMID])
GO
