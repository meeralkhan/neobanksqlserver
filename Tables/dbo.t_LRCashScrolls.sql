CREATE TABLE [dbo].[t_LRCashScrolls]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[ScrollNo] [decimal] (24, 0) NOT NULL,
[Amount] [money] NOT NULL
)
GO
ALTER TABLE [dbo].[t_LRCashScrolls] ADD CONSTRAINT [PK_t_LRCashScrolls] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [wDate], [ScrollNo])
GO
