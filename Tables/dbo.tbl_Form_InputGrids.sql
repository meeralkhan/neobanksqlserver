CREATE TABLE [dbo].[tbl_Form_InputGrids]
(
[frmName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[frmToLink] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabId] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PortletReq] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PortletTitle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PortletIcon] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PortletColor] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[tbl_Form_InputGrids] ADD CONSTRAINT [PK_tbl_Form_InputGrids] PRIMARY KEY CLUSTERED ([frmName], [frmToLink])
GO
