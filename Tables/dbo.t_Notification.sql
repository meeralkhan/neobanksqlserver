CREATE TABLE [dbo].[t_Notification]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[AccountId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[wDate] [datetime] NULL,
[AccountType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [decimal] (21, 3) NULL,
[ClientId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNo] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountBalance] [decimal] (21, 3) NULL,
[TemplateId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[countryISOCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestParameter] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestResponse] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSend] [bit] NULL,
[UpdateTime] [datetime] NULL,
[ChannelRefId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DescriptionID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForeignAmount] [decimal] (21, 3) NULL,
[ExchangeRate] [decimal] (18, 6) NULL,
[SenderBeneName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_Notification] ADD CONSTRAINT [PK_t_Notification] PRIMARY KEY CLUSTERED ([Id])
GO
