CREATE TABLE [dbo].[t_CIF_BoardDirectors]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BoardDirectorID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_CIF_BoardDirectors] ADD CONSTRAINT [PK_t_CIF_BoardDirectors] PRIMARY KEY CLUSTERED ([OurBranchID], [ClientID], [BoardDirectorID])
GO
