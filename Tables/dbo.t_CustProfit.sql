CREATE TABLE [dbo].[t_CustProfit]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ReferenceNo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValueDate] [datetime] NOT NULL,
[ProfitDate] [datetime] NOT NULL,
[Amount] [decimal] (18, 6) NOT NULL,
[LAccount] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Flag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProfitAmount] [decimal] (18, 6) NULL,
[TaxAmount] [decimal] (18, 6) NULL
)
GO
ALTER TABLE [dbo].[t_CustProfit] ADD CONSTRAINT [PK_t_CustProfit] PRIMARY KEY CLUSTERED ([SerialNo])
GO
