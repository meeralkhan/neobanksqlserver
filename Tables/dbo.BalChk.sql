CREATE TABLE [dbo].[BalChk]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastUpdateTime] [datetime] NOT NULL,
[ClearBalance] [decimal] (21, 3) NOT NULL,
[Limit] [decimal] (21, 3) NOT NULL,
[Effects] [decimal] (21, 3) NOT NULL,
[Shadow] [decimal] (21, 3) NOT NULL,
[FreezeAmt] [decimal] (21, 3) NOT NULL,
[MinBalance] [decimal] (21, 3) NOT NULL,
[TotalBalance] [decimal] (21, 3) NOT NULL,
[AvailableBalance] [decimal] (21, 3) NOT NULL
)
GO
ALTER TABLE [dbo].[BalChk] ADD CONSTRAINT [PK_BalChk] PRIMARY KEY CLUSTERED ([SerialNo])
GO
