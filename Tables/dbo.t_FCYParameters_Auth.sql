CREATE TABLE [dbo].[t_FCYParameters_Auth]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FCYCriteriaID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParamID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TreasuryBranchID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DReversalNarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CReversalNarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DReversalMemo] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CReversalMemo] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DPositionTrNarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPositionTrNarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DPositionTrMemo] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPositionTrMemo] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DFCYIntBrNarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CFCYIntBrNarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DFCYIntBrMemo] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CFCYIntBrMemo] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DLocalIntBrNarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLocalIntBrNarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DLocalIntBrMemo] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLocalIntBrMemo] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FCYIntBrGLNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocalIntBrGLNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FCYPositionGLNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LCYPositionGLNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_FCYParameters_Auth] ADD CONSTRAINT [PK_t_FCYParameters_Auth] PRIMARY KEY CLUSTERED ([OurBranchID], [FCYCriteriaID], [ParamID])
GO
