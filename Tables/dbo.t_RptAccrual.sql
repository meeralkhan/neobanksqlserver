CREATE TABLE [dbo].[t_RptAccrual]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalProduct] [money] NULL,
[AverageBalance] [money] NULL,
[Profit] [money] NULL,
[Tax] [money] NULL,
[Rate] [money] NULL,
[wDate] [datetime] NULL,
[PrevAccrualAmount] [money] NULL,
[PrevAccrualDate] [datetime] NULL
)
GO
