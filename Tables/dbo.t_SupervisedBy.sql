CREATE TABLE [dbo].[t_SupervisedBy]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Date] [datetime] NULL,
[ScrollNo] [numeric] (10, 0) NULL,
[SerialNo] [int] NULL,
[ChequeID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearBalance] [money] NULL,
[Effects] [money] NULL,
[Limit] [money] NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [money] NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionCategory] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RemotePass] [bit] NULL
)
GO
