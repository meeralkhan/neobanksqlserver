CREATE TABLE [dbo].[t_FDBalanceHistory]
(
[wDate] [datetime] NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecieptID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [money] NULL,
[ExchangeRate] [decimal] (18, 6) NULL,
[IssueDate] [datetime] NULL,
[StartDate] [datetime] NULL,
[MaturityDate] [datetime] NULL,
[Rate] [money] NULL,
[Interest] [money] NOT NULL,
[InterestPaid] [money] NOT NULL,
[InterestPaidupTo] [datetime] NULL,
[Accrual] [money] NOT NULL,
[tAccrual] [money] NOT NULL,
[AccrualUpto] [datetime] NULL
)
GO
ALTER TABLE [dbo].[t_FDBalanceHistory] ADD CONSTRAINT [PK_t_FDBalanceHistory] PRIMARY KEY CLUSTERED ([wDate], [OurBranchID], [AccountID], [RecieptID])
GO
