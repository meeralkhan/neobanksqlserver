CREATE TABLE [dbo].[Quantum_BRF_34_35]
(
[NAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FACE_VALUE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BOR_INVEST] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRYCODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ToolRunDate] [datetime] NULL
)
GO
