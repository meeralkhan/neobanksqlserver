CREATE TABLE [dbo].[InterBranchFailedDump]
(
[UniqueID] [nvarchar] (450) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (21, 3) NOT NULL,
[ExchangeRate] [decimal] (18, 6) NOT NULL,
[LocalEq] [decimal] (21, 3) NOT NULL,
[NarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxMethod] [int] NOT NULL,
[SystemDateTime] [datetime] NOT NULL,
[TransitAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MismatchAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChannelRefId] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxMemo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxNarrationId] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[InterBranchFailedDump] ADD CONSTRAINT [PK_InterBranchFailedDump] PRIMARY KEY CLUSTERED ([UniqueID])
GO
