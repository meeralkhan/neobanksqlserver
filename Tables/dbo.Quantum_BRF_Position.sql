CREATE TABLE [dbo].[Quantum_BRF_Position]
(
[DEAL_NO] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENTITY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INSTRUMENT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSN_DT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FACE_VALUE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BOOK_VALUE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MKT_VALUE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RPTNO] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECISSUE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONTRYCODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTYPECODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCCODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCNAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INDUSTRY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENTID] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ToolRunDate] [datetime] NULL
)
GO
