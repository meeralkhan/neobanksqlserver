CREATE TABLE [dbo].[t_CIF_AuthSignatory]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AuthorizedSignatoryID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_CIF_AuthSignatory] ADD CONSTRAINT [PK_t_CIF_AuthSignatory] PRIMARY KEY CLUSTERED ([OurBranchID], [ClientID], [AuthorizedSignatoryID])
GO
