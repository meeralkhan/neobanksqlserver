CREATE TABLE [dbo].[t_ATM_TimeLog]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[Activity] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActivityStartTime] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActivityEndTime] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActivityTime] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
