CREATE TABLE [dbo].[tbl_ReportsGuids]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[GUID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateTime] [datetime] NULL,
[ReportName] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
