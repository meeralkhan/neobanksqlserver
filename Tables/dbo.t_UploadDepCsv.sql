CREATE TABLE [dbo].[t_UploadDepCsv]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ReferenceNo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Dated] [datetime] NOT NULL,
[BranchID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountBalance] [decimal] (21, 3) NOT NULL,
[AccountStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MinSlabAmount] [decimal] (18, 6) NULL,
[MaxSlabAmount] [decimal] (18, 6) NULL,
[CloseDate] [datetime] NULL,
[CloseRefNo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[ClosingBalance] [decimal] (21, 3) NULL,
[ValueDate] [datetime] NULL,
[ProfitAmount] [decimal] (21, 3) NULL,
[LinkAccount] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProfitUpto] [datetime] NULL
)
GO
ALTER TABLE [dbo].[t_UploadDepCsv] ADD CONSTRAINT [PK_t_UploadDepCsv] PRIMARY KEY CLUSTERED ([SerialNo])
GO
