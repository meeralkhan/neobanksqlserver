CREATE TABLE [dbo].[t_AccrualHistory]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecieptID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Date] [datetime] NOT NULL,
[SerialNo] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[FromDate] [datetime] NOT NULL,
[ToDate] [datetime] NOT NULL,
[RateType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rate] [decimal] (24, 6) NOT NULL,
[Days] [int] NOT NULL,
[PerDay] [money] NOT NULL,
[SubTotal] [money] NOT NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_AccrualHistory] ADD CONSTRAINT [PK_t_AccrualHistory] PRIMARY KEY CLUSTERED ([OurBranchID], [RecieptID], [Date], [SerialNo])
GO
