CREATE TABLE [dbo].[t_VerifyApi]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ControllerName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsVerify] [bit] NULL
)
GO
ALTER TABLE [dbo].[t_VerifyApi] ADD CONSTRAINT [PK_t_VerifyApi] PRIMARY KEY CLUSTERED ([Id])
GO
