CREATE TABLE [dbo].[t_CashDenomination]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[OpeningBalance] [money] NOT NULL,
[Receipts] [money] NULL,
[Payments] [money] NULL,
[NoOfReceipts] [int] NULL,
[NoOfPayments] [int] NULL,
[N10000] [int] NULL,
[N5000] [int] NULL,
[N1000] [int] NULL,
[N500] [int] NULL,
[N200] [int] NULL,
[N100] [int] NULL,
[N50] [int] NULL,
[N20] [int] NULL,
[N10] [int] NULL,
[N5] [int] NULL,
[N2] [int] NULL,
[N1] [int] NULL,
[AmtN10000] [money] NULL,
[AmtN5000] [money] NULL,
[AmtN1000] [money] NULL,
[AmtN500] [money] NULL,
[AmtN200] [money] NULL,
[AmtN100] [money] NULL,
[AmtN50] [money] NULL,
[AmtN20] [money] NULL,
[AmtN10] [money] NULL,
[AmtN5] [money] NULL,
[AmtN2] [money] NULL,
[AmtN1] [money] NULL,
[C5] [int] NULL,
[C2] [int] NULL,
[C1] [int] NULL,
[CP5] [int] NULL,
[CP25] [int] NULL,
[CP10] [int] NULL,
[AmtC5] [money] NULL,
[AmtC2] [money] NULL,
[AmtC1] [money] NULL,
[AmtCP5] [money] NULL,
[AmtCP25] [money] NULL,
[AmtCP10] [money] NULL,
[TotalNotes] [int] NULL,
[TotalCoins] [int] NULL,
[ClosingBalance] [money] NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_CashDenomination] ADD CONSTRAINT [PK_t_CashDenomination] PRIMARY KEY CLUSTERED ([OurBranchID], [CurrencyID], [wDate])
GO
