CREATE TABLE [dbo].[t_FacilityProduct_Auth]
(
[SerialID] [decimal] (24, 0) NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FacilityID] [decimal] (24, 0) NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LimitType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Limit] [money] NULL,
[Status] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_FacilityProduct_Auth] ADD CONSTRAINT [PK_t_FacilityProduct_Auth] PRIMARY KEY CLUSTERED ([SerialID], [OurBranchID], [ClientID], [FacilityID], [ProductID])
GO
