CREATE TABLE [dbo].[t_EODPnL]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialID] [int] NOT NULL IDENTITY(1, 1),
[KOGL] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContraGL] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GainNarrationID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GainMemo] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LossNarrationID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LossMemo] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [int] NULL
)
GO
ALTER TABLE [dbo].[t_EODPnL] ADD CONSTRAINT [PK_t_EODPnL] PRIMARY KEY CLUSTERED ([OurBranchID], [SerialID])
GO
