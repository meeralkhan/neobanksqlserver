CREATE TABLE [dbo].[t_ATM_NonTariffBranches]
(
[BranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_ATM_NonTariffBranches] ADD CONSTRAINT [PK_t_ATM_NonTariffBranches] PRIMARY KEY CLUSTERED ([BranchID])
GO
