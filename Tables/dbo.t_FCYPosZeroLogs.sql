CREATE TABLE [dbo].[t_FCYPosZeroLogs]
(
[OurBranchID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ID] [int] NOT NULL IDENTITY(1, 1),
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScrollNo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsError] [bit] NOT NULL,
[Remarks] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[wDate] [datetime] NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_FCYPosZeroLogs] ADD CONSTRAINT [PK_t_FCYPosZeroLogs] PRIMARY KEY CLUSTERED ([OurBranchID], [ID])
GO
