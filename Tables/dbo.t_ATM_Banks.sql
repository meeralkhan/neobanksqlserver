CREATE TABLE [dbo].[t_ATM_Banks]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankIMD] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankShortName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ODLimitAllow] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[t_ATM_Banks] ADD CONSTRAINT [PK_t_ATM_Banks] PRIMARY KEY CLUSTERED ([OurBranchID], [BankIMD])
GO
