CREATE TABLE [dbo].[t_MMSecuritySaleHistory]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealNo] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [int] NOT NULL,
[P_DealNo] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[S_Amount] [money] NOT NULL,
[SecurityID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[S_IntRecv] [money] NOT NULL,
[S_PreRecvPaid] [money] NOT NULL,
[P_IntPaid] [money] NOT NULL,
[P_PreRecvPaid] [money] NOT NULL,
[IntRecvd] [money] NOT NULL,
[IsPledge] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[P_AmtExp] [money] NOT NULL
)
GO
