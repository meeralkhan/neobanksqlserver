CREATE TABLE [dbo].[tbl_Form_Tabs]
(
[frmName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabIcon] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TitleEN] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TitleAR] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TitleUR] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderNo] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[tbl_Form_Tabs] ADD CONSTRAINT [PK_tbl_Form_Tabs] PRIMARY KEY CLUSTERED ([frmName], [TabId])
GO
