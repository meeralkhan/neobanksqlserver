CREATE TABLE [dbo].[t_MapDrive_Auth]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FormToRun] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GLID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Program] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExtraDetails] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreditPostingLimit] [money] NOT NULL,
[DebitPostingLimit] [money] NOT NULL,
[CreditVerifyLimit] [money] NOT NULL,
[DebitVerifyLimit] [money] NOT NULL,
[CreditSupervisionLimit] [money] NOT NULL,
[DebitSupervisionLimit] [money] NOT NULL,
[FullAccess] [bit] NOT NULL,
[AddAccess] [bit] NOT NULL,
[DeleteAccess] [bit] NOT NULL,
[EditAccess] [bit] NOT NULL,
[VerifyAccess] [bit] NOT NULL,
[AuthorizeAccess] [bit] NOT NULL,
[Authorization] [int] NOT NULL,
[Auth] [int] NULL,
[COperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Day] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[a] [int] NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_MapDrive_Auth] ADD CONSTRAINT [PK_t_MapDrive_Auth] PRIMARY KEY CLUSTERED ([OurBranchID], [OperatorID], [FormToRun])
GO
