CREATE TABLE [dbo].[t_QueuePermission]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ApiName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueueName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsAllow] [bit] NULL
)
GO
