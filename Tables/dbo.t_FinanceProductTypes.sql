CREATE TABLE [dbo].[t_FinanceProductTypes]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinanceProductType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReceivableProduct] [int] NULL,
[CapitalizedProduct] [int] NULL,
[TransactionACProduct] [int] NULL,
[FirstSearchProduct] [int] NULL,
[SecondSearchProduct] [int] NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccDeprProduct] [int] NULL,
[IstasnaAccProduct] [int] NULL,
[MarginAccProduct] [int] NULL,
[AccruedProfitProduct] [int] NULL,
[PrePaidInsuranceProduct] [int] NULL,
[DisbProduct] [int] NULL
)
GO
ALTER TABLE [dbo].[t_FinanceProductTypes] ADD CONSTRAINT [PK_t_FinanceProductTypes] PRIMARY KEY CLUSTERED ([OurBranchID], [FinanceProductType])
GO
