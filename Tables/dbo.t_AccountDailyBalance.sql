CREATE TABLE [dbo].[t_AccountDailyBalance]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Wdate] [datetime] NOT NULL,
[Amount] [money] NOT NULL
)
GO
ALTER TABLE [dbo].[t_AccountDailyBalance] ADD CONSTRAINT [PK_t_AccountDailyBalance] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [ProductID], [Wdate])
GO
