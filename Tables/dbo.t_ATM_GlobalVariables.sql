CREATE TABLE [dbo].[t_ATM_GlobalVariables]
(
[Version] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CheckLimit] [bit] NOT NULL,
[TaxDeduction] [money] NOT NULL,
[TaxLimit] [money] NOT NULL,
[TaxAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UBPS_73_Allowed] [bit] NOT NULL,
[TitleFetch_62] [bit] NOT NULL,
[3PFT_40] [bit] NOT NULL,
[IBFT_48] [bit] NOT NULL,
[CashDeposit_15] [bit] NOT NULL,
[CreditCardBill_22] [bit] NOT NULL,
[NonTariff] [bit] NOT NULL,
[LocalCurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LocalCurrency] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BalanceUploaded] [bit] NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AppDomain] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_ATM_GlobalVariables] ADD CONSTRAINT [PK_t_ATM_GlobalVariables] PRIMARY KEY CLUSTERED ([OurBranchID])
GO
