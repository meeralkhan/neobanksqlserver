CREATE TABLE [dbo].[t_MMSecurityTransaction]
(
[RefNo] [smallint] NOT NULL,
[DealNo] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PartyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SecurityID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[CloseDate] [datetime] NOT NULL
)
GO
