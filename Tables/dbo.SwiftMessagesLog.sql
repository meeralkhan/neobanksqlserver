CREATE TABLE [dbo].[SwiftMessagesLog]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[Json_Message] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message_Type] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reference] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Txn_Id] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Submitted_At] [datetime] NULL,
[SQLPostedDate] [datetime] NULL
)
GO
ALTER TABLE [dbo].[SwiftMessagesLog] ADD CONSTRAINT [PK_SwiftMessagesLog] PRIMARY KEY CLUSTERED ([Id])
GO
