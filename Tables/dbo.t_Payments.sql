CREATE TABLE [dbo].[t_Payments]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PaymentID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialID] [int] NOT NULL,
[wDate] [datetime] NOT NULL,
[ValueDate] [datetime] NOT NULL,
[TrxMode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScrollNo] [int] NOT NULL,
[SerialNo] [int] NOT NULL,
[OperatorID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupervisorID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_Payments] ADD CONSTRAINT [PK_t_Payments_1] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [DealID], [PaymentID], [SerialID])
GO
