CREATE TABLE [dbo].[t_CIF_ShareHolders]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ShareHolderID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_CIF_ShareHolders] ADD CONSTRAINT [PK_t_CIF_ShareHolders] PRIMARY KEY CLUSTERED ([OurBranchID], [ClientID], [ShareHolderID])
GO
