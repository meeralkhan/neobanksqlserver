CREATE TABLE [dbo].[t_UploadedFiles]
(
[UploadID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UploadPath] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_UploadedFiles] ADD CONSTRAINT [PK_t_UploadedFiles] PRIMARY KEY CLUSTERED ([UploadID])
GO
