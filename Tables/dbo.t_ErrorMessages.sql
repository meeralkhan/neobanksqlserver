CREATE TABLE [dbo].[t_ErrorMessages]
(
[ErrorCode] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ErrorMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalCode] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isError] [bit] NULL,
[data] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_ErrorMessages] ADD CONSTRAINT [PK_ErrorMessages] PRIMARY KEY CLUSTERED ([ErrorCode])
GO
