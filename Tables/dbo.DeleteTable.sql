CREATE TABLE [dbo].[DeleteTable]
(
[BankName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankBranch] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [int] NULL
)
GO
