CREATE TABLE [dbo].[tbl_ReportsMain]
(
[ReportId] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PkTable] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Alias] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Columns] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Condition] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderBy] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[tbl_ReportsMain] ADD CONSTRAINT [PK_tbl_ReportsMain] PRIMARY KEY CLUSTERED ([ReportId])
GO
