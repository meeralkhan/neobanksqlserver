CREATE TABLE [dbo].[t_Adv_Disb_Accrual_Auth]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [decimal] (24, 0) NOT NULL,
[ColumnID] [decimal] (24, 0) NOT NULL,
[AccrualDate] [datetime] NULL,
[ProfitRate] [money] NULL,
[NoOfDays] [int] NULL,
[AccrualAmount] [money] NULL,
[ReversalAmount] [money] NULL,
[DifferenceDate] [datetime] NULL,
[DifferenceDays] [int] NULL,
[DifferenceAmount] [money] NULL,
[InsuranceAmount1] [money] NULL,
[ChargesAmount1] [money] NULL,
[InsuranceAmount2] [money] NULL,
[ChargesAmount2] [money] NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_Adv_Disb_Accrual_Auth] ADD CONSTRAINT [PK_t_Adv_Disb_Accrual_Auth] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [DealID], [ColumnID])
GO
