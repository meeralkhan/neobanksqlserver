CREATE TABLE [dbo].[t_MultiBranchLogs]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ChannelRefid] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorMessage] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExceptionMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Module] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApiName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestParameter] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestResponse] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateBy] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedOn] [datetime] NOT NULL
)
GO
