CREATE TABLE [dbo].[t_Signature]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialID] [decimal] (18, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Picture] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[wDate] [datetime] NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastModifyDate] [datetime] NOT NULL,
[ComputerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastUpdateTerminal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastUpdateUser] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_Signature] ADD CONSTRAINT [PK_t_Signature] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [SerialID])
GO
