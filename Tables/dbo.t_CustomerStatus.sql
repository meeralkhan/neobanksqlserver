CREATE TABLE [dbo].[t_CustomerStatus]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GLID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScrollNo] [numeric] (10, 0) NULL,
[SerialNo] [int] NULL,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [money] NULL,
[Rate] [money] NULL,
[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsOverDraft] [bit] NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TellerMessage] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TellerNewMessage] [bit] NOT NULL,
[SupervisorMessage] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupervisorNewMessage] [bit] NOT NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
