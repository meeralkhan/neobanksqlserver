CREATE TABLE [dbo].[t_MobileMsgs]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[MobileNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [varchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateTime] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[t_MobileMsgs] ADD CONSTRAINT [PK_t_MobileMsgs] PRIMARY KEY CLUSTERED ([SerialNo])
GO
