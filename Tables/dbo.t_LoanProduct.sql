CREATE TABLE [dbo].[t_LoanProduct]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[mFrqMonthly] [datetime] NULL,
[mFrqQtrly] [datetime] NULL,
[mFrqHalfYearly] [datetime] NULL,
[mFrqYearly] [datetime] NULL,
[AutoApplyOnDate] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Catagory] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReceivableProdcutID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CapitalizedProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NominatedProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NominatedProductID1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NominatedProductID2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDeclareAmountPosted] [bit] NULL
)
GO
