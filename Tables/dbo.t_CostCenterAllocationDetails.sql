CREATE TABLE [dbo].[t_CostCenterAllocationDetails]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CostCenterID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[S10001] [decimal] (18, 5) NULL,
[S20001] [decimal] (18, 5) NULL,
[S30001] [decimal] (18, 5) NULL,
[S40001] [decimal] (18, 5) NULL,
[S50001] [decimal] (18, 5) NULL,
[PE10001] [decimal] (18, 5) NULL,
[PE20001] [decimal] (18, 5) NULL,
[PE30001] [decimal] (18, 5) NULL,
[PE40001] [decimal] (18, 5) NULL,
[PE50001] [decimal] (18, 5) NULL,
[O10001] [decimal] (18, 5) NULL,
[O20001] [decimal] (18, 5) NULL,
[O30001] [decimal] (18, 5) NULL,
[O40001] [decimal] (18, 5) NULL,
[O50001] [decimal] (18, 5) NULL
)
GO
ALTER TABLE [dbo].[t_CostCenterAllocationDetails] ADD CONSTRAINT [PK_t_CostCenterAllocationDetails] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [CostCenterID])
GO
