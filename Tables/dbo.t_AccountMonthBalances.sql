CREATE TABLE [dbo].[t_AccountMonthBalances]
(
[Month] [datetime] NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearBalance] [money] NULL,
[Effects] [money] NULL,
[LocalClearBalance] [money] NULL,
[LocalEffects] [money] NULL,
[PrevMonthBalance] [money] NULL
)
GO
ALTER TABLE [dbo].[t_AccountMonthBalances] ADD CONSTRAINT [PK_t_AccountMonthBalances] PRIMARY KEY CLUSTERED ([Month], [OurBranchID], [AccountID])
GO
