CREATE TABLE [dbo].[t_ATM_TransactionLog]
(
[OurBranchId] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [numeric] (18, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[IDate] [datetime] NOT NULL,
[IMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ODate] [datetime] NOT NULL,
[OMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Remarks] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ErrorQueryString] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ErrorErrDesc] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_ATM_TransactionLog] ADD CONSTRAINT [PK_t_ATM_TransactionLog] PRIMARY KEY CLUSTERED ([OurBranchId], [SerialNo])
GO
