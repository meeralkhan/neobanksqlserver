CREATE TABLE [dbo].[t_FXPositionCriteria]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FXCriteriaID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EntryType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitOurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitCurrencyID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DebitNarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditOurBranchID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCurrencyID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditNarrationID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_FXPositionCriteria] ADD CONSTRAINT [PK_t_FXPositionCriteria] PRIMARY KEY CLUSTERED ([OurBranchID], [FXCriteriaID])
GO
