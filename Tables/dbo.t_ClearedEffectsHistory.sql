CREATE TABLE [dbo].[t_ClearedEffectsHistory]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScrollNo] [int] NOT NULL,
[SerialNo] [decimal] (10, 0) NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [char] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValueDate] [datetime] NOT NULL,
[wDate] [datetime] NOT NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeDate] [datetime] NOT NULL,
[Amount] [decimal] (18, 6) NOT NULL,
[ForeignAmount] [decimal] (18, 6) NOT NULL,
[ExchangeRate] [decimal] (18, 6) NOT NULL,
[BankCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_ClearedEffectsHistory] ADD CONSTRAINT [PK_t_ClearedEffectsHistory] PRIMARY KEY CLUSTERED ([OurBranchID], [RefNo])
GO
