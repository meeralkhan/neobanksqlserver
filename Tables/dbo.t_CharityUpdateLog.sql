CREATE TABLE [dbo].[t_CharityUpdateLog]
(
[OurBranchID] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CharityProcessDate] [datetime] NOT NULL,
[UpdateDate] [datetime] NULL
)
GO
ALTER TABLE [dbo].[t_CharityUpdateLog] ADD CONSTRAINT [PK_t_CharityUpdateLog] PRIMARY KEY CLUSTERED ([OurBranchID], [CharityProcessDate])
GO
