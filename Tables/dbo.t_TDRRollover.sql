CREATE TABLE [dbo].[t_TDRRollover]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1),
[wDate] [datetime] NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReceiptID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransferScrollNo] [decimal] (24, 0) NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsUnderLien] [int] NOT NULL,
[TotalLienAmount] [decimal] (21, 3) NOT NULL,
[NewReceiptID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[MaturityDate] [datetime] NULL,
[Amount] [decimal] (21, 3) NULL,
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_TDRRollover] ADD CONSTRAINT [PK_t_TDRRollover] PRIMARY KEY CLUSTERED ([SerialNo])
GO
