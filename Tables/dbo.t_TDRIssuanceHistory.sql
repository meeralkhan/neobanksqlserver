CREATE TABLE [dbo].[t_TDRIssuanceHistory]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [decimal] (18, 0) NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecieptID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAccountType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAccount] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [money] NULL,
[ExchangeRate] [decimal] (18, 6) NULL,
[IssueDate] [datetime] NULL,
[StartDate] [datetime] NULL,
[MaturityDate] [datetime] NULL,
[Rate] [money] NULL,
[AutoProfit] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentPeriod] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Treatment] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AutoRollover] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OptionsAtMaturity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPremiumRate] [int] NULL,
[AdditionalData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefNo] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsUnderLien] [int] NULL,
[IsLost] [int] NULL,
[LostRemarks] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Interest] [money] NOT NULL,
[InterestPaid] [money] NOT NULL,
[InterestPaidupTo] [datetime] NULL,
[IsClosed] [int] NULL,
[CloseDate] [datetime] NULL,
[Accrual] [money] NOT NULL,
[AccrualUpto] [datetime] NULL,
[tAccrual] [money] NOT NULL,
[Penalty] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PenaltyRateType] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PenaltyAmountRate] [money] NULL,
[TotalPenaltyAmount] [money] NULL,
[PartialAmount] [money] NULL,
[TotalLienAmount] [decimal] (21, 3) NULL,
[OldReceiptID] [decimal] (24, 0) NULL,
[ParentReceiptID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartialWithdrawRemarks] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CloseRemarks] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PartialDate] [datetime] NULL,
[NoOfDays] [int] NULL,
[PartialInterestAmount] [money] NULL
)
GO
ALTER TABLE [dbo].[t_TDRIssuanceHistory] ADD CONSTRAINT [PK_t_TDRIssuanceHistory] PRIMARY KEY CLUSTERED ([SerialNo], [OurBranchID], [RecieptID])
GO
