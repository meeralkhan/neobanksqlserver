CREATE TABLE [dbo].[t_GLModelTransaction]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RefNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VoucherID] [decimal] (10, 0) NOT NULL,
[SerialID] [decimal] (10, 0) NOT NULL,
[Date] [datetime] NOT NULL,
[ValueDate] [datetime] NULL,
[DescriptionID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [money] NULL,
[ForeignAmount] [money] NULL,
[ExchangeRate] [decimal] (18, 6) NULL,
[IsCredit] [bit] NULL,
[TransactionType] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionMethod] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Indicator] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdditionalData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClearingType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RegisterDate] [datetime] NULL
)
GO
