CREATE TABLE [dbo].[sysdiagrams]
(
[name] [sys].[sysname] NOT NULL,
[principal_id] [int] NOT NULL,
[diagram_id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[version] [int] NULL,
[definition] [varbinary] (max) NULL
)
GO
ALTER TABLE [dbo].[sysdiagrams] ADD CONSTRAINT [PK__sysdiagr__C2B05B61D3BCD72D] PRIMARY KEY CLUSTERED ([diagram_id])
GO
