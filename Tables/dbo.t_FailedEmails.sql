CREATE TABLE [dbo].[t_FailedEmails]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eSerialNo] [decimal] (24, 0) NOT NULL,
[SerialNo] [decimal] (24, 0) NOT NULL,
[ErrorMessage] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[eDateTime] [datetime] NOT NULL,
[eTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_FailedEmails] ADD CONSTRAINT [PK_t_FailedEmails] PRIMARY KEY CLUSTERED ([OurBranchID], [eSerialNo], [SerialNo])
GO
