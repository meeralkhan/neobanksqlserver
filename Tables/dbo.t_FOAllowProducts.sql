CREATE TABLE [dbo].[t_FOAllowProducts]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_FOAllowProducts] ADD CONSTRAINT [PK_t_FOAllowProducts] PRIMARY KEY CLUSTERED ([OurBranchID], [ProductID])
GO
