CREATE TABLE [dbo].[tbl_Menu]
(
[MenuId] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LevelId] [int] NOT NULL,
[ParentId] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label_EN] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label_AR] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Label_UR] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MenuType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LinkId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TabIcon] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OrderNo] [int] NOT NULL,
[NewWindow] [int] NULL,
[frmOnDisplay] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tbl_Menu] ADD CONSTRAINT [PK_tbl_Menu] PRIMARY KEY CLUSTERED ([MenuId])
GO
