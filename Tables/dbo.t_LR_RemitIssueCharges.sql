CREATE TABLE [dbo].[t_LR_RemitIssueCharges]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IssueSerialNo] [numeric] (18, 0) NOT NULL,
[ColumnID] [numeric] (18, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ChargeID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DrawnBankID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DrawnBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InstrumentType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChargeName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PayeeName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BeneficiaryName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LRAmount] [money] NOT NULL,
[Charges] [money] NOT NULL,
[wDate] [datetime] NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_LR_RemitIssueCharges] ADD CONSTRAINT [PK_t_LR_RemitIssueCharges_1] PRIMARY KEY CLUSTERED ([OurBranchID], [IssueSerialNo], [ColumnID], [ChargeID])
GO
