CREATE TABLE [dbo].[t_Transactions]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScrollNo] [numeric] (18, 0) NOT NULL,
[SerialNo] [int] NOT NULL,
[RefNo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[AccountType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DocumentType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProductID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ValueDate] [datetime] NOT NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeDate] [datetime] NULL,
[Amount] [money] NOT NULL,
[ForeignAmount] [money] NOT NULL,
[ExchangeRate] [decimal] (16, 8) NULL,
[ProfitLoss] [money] NOT NULL,
[MeanRate] [money] NOT NULL,
[DescriptionID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BankCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TheirAccount] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtraDetails] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChequeDigit] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VoucherCode] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReturnCode] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DrawerOrPayee] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxPrinted] [bit] NOT NULL,
[GLID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsLocalCurrency] [bit] NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UtilityNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdditionalData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsMainTrx] [smallint] NOT NULL,
[DocType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GLVoucherID] [decimal] (24, 0) NOT NULL,
[AppWHTax] [smallint] NOT NULL,
[WHTaxAmount] [money] NOT NULL,
[WHTaxMode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WHTScrollNo] [int] NOT NULL,
[SourceBranch] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TargetBranch] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClearingType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RegisterDate] [datetime] NULL,
[ChqStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChqSuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChqSuperviseTime] [datetime] NULL,
[ChqSuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Remarks] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MerchantType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreditCardNumber] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[STAN] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CompanyNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChannelId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VirtualAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VirtualAccountTitle] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChannelRefID] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConvRate] [decimal] (15, 9) NULL,
[CurrCodeTran] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcqCountryCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrCodeSett] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlmntAmount] [money] NULL,
[ATMStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSEntryMode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSConditionCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSPINCaptCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcqInstID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RetRefNum] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthIDResp] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardAccptID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardAccptNameLoc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VISATrxID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxTimeStamp] [datetime] NOT NULL,
[TrxDesc] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MCC] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CAVV] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForwardInstID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrevDayEntry] [int] NULL,
[ClearingRefNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChargeID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VatID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Percent] [money] NULL,
[RejectRemarks] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrevYearEntry] [int] NULL,
[TClientId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAccountid] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TProductId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CostCenterID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLVendorID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[memo2] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VatReferenceID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 CREATE TRIGGER [dbo].[tr_Transactions] ON [dbo].[t_Transactions]              
AFTER INSERT              
AS              
BEGIN            
--CashBack            
   declare @OurBranchID nvarchar(30)              
   declare @AccountID nvarchar(30)              
   declare @TrType nvarchar(1)              
   declare @wDate datetime              
   declare @AccountName nvarchar(100)              
   declare @ProductID nvarchar(30)              
   declare @CurrencyID nvarchar(30)              
   declare @Amount decimal(21,3)              
   declare @ForeignAmount decimal(21,3)              
   declare @ExchangeRate decimal(18,6)              
   declare @DescriptionID nvarchar(30)              
   declare @ChannelRefID nvarchar(100)              
   declare @TrxTimeStamp datetime              
   declare @CashbackPercent money            
   declare @CcyRounding int            
   declare @CashbackAmount money            
   declare @BalanceAmount money            
   declare @BeneficiaryName varchar(50)          
            
   select @OurBranchID=OurBranchID, @AccountID=AccountID, @TrType=TrxType, @wDate=wDate, @AccountName=AccountName,            
   @ProductID=ProductID, @CurrencyID=CurrencyID, @Amount=Amount, @ForeignAmount=ForeignAmount, @ExchangeRate=ExchangeRate,            
   @DescriptionID=DescriptionID, @ChannelRefID = ChannelRefID, @TrxTimeStamp = TrxTimeStamp            
   from inserted            
            
   if exists(select CriteriaID from t_CashbackCriteria where OurBranchID = @OurBranchID and ProductID = @ProductID and DescriptionID = @DescriptionID)            
   begin            
                  
  select @CashbackPercent = Cashback from t_CashbackCriteria            
  where OurBranchID = @OurBranchID and ProductID = @ProductID and DescriptionID = @DescriptionID            
            
  if (@CurrencyID = 'AED')            
  begin            
  select @CcyRounding = 2;            
 set @CashbackAmount = ROUND(@Amount * @CashbackPercent/100 , @CcyRounding);            
  end            
  else            
  begin            
  select @CcyRounding = ISNULL(CRRounding,2) from t_Currencies where OurBranchID = @OurBranchID and CurrencyID = @CurrencyID;            
 set @CashbackAmount = ROUND(@ForeignAmount * @CashbackPercent/100 , @CcyRounding);            
  end            
             
  insert into t_Cashback321 (OurBranchID, AccountID, AccountName, ProductID, CurrencyID, TrType, wDate, ForeignAmount,             
  ExchangeRate, Amount, CashbackPercent, CashbackAmount, DescriptionID, ChannelRefID, TrxTimeStamp, CreateBy, CreateTime, IsPaid)            
  values (@OurBranchID, @AccountID, @AccountName, @ProductID, @CurrencyID, @TrType, @wDate, @ForeignAmount,             
  @ExchangeRate, @Amount, @CashbackPercent, @CashbackAmount, @DescriptionID, @ChannelRefID, @TrxTimeStamp, 'SYSTEM', GETDATE(), 0)            
                  
    end              
            
    --Charity            
                
   SET NOCOUNT ON            
            
   set @OurBranchID =''            
   set @AccountID =''            
   set @TrType =''            
   declare @TrxType nvarchar(30)            
   declare @AccountType nvarchar(1)            
   set @AccountName = ''            
   set @ProductID = ''            
   declare @bCurrencyID nvarchar(30)            
   set @CurrencyID = ''            
   declare @ChequeID nvarchar(30)            
   declare @ChequeDate datetime            
   set @Amount = 0            
   declare @chAmount decimal(21,3)            
   declare @cAmount decimal(21,3)            
   set @ForeignAmount = 0            
   set @ExchangeRate = 0            
   set @DescriptionID = ''            
   declare @Description nvarchar(255)            
   set @ChannelRefID = ''            
   set @TrxTimeStamp = null            
   declare @SMSText nvarchar(max)            
   declare @SMSToSend nvarchar(max)            
   declare @CharityGL nvarchar(30)            
   declare @BankID nvarchar(30)            
            
   select @OurBranchID=OurBranchID, @AccountID=AccountID, @TrType=TrxType, @TrxType=case TrxType when 'D' then 'Debited' else 'Credited' end, @wDate=wDate, @AccountType='Customer', @AccountName=AccountName,            
   @chAmount = 0, @ProductID=ProductID, @CurrencyID=CurrencyID, @ChequeID=ChequeID, @ChequeDate=ChequeDate, @Amount=Amount, @ForeignAmount=ForeignAmount,            
   @ExchangeRate=ExchangeRate, @DescriptionID = DescriptionID, @Description = Description, @ChannelRefID = ChannelRefID, @TrxTimeStamp = TrxTimeStamp            
   from inserted            
            
   if @TrType='D' and @AccountType='C'            
   begin            
   if exists(select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and ISNULL(IsCharity,0) = 1)            
   begin            
                  
  if exists(select AccountID from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID and ISNULL(IsCharity,0) = 1)            
  begin            
                
 select @cAmount = ISNULL(CharityAmount,1) from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID and ISNULL(IsCharity,0) = 1            
 select @bCurrencyID = LocalCurrency, @BankID = OurBankID from t_GLobalVariables where OurBranchID = @OurBranchID            
            
 if @CurrencyID = @bCurrencyID            
 begin            
    SELECT @chAmount = @cAmount - (@Amount % @cAmount)            
 end            
 else            
 begin            
    SELECT @chAmount = @cAmount - (@ForeignAmount % @cAmount)            
 end            
            
 if @chAmount > 0            
 begin            
    if exists (select top 1 GLAccountID from t_CharityGLs where OurBranchID=@OurBranchID and CurrencyID=@CurrencyID)            
 begin            
              
   select @CharityGL = GLAccountID from t_CharityGLs where OurBranchID=@OurBranchID and CurrencyID=@CurrencyID            
              
   declare @MaxScrollNo decimal(24,0)            
   declare @ClearBalance money            
               declare @Limit money            
               declare @FreezeAmount money            
               declare @ProductMinBalance money            
               declare @DrawableBalance money            
                           
               --exec @Limit = fnc_GetLimit @OurBranchID, @AccountID            
                           
               SELECT @ClearBalance = ab.ClearBalance, @Limit = Limit, @FreezeAmount = ab.FreezeAmount, @ProductMinBalance = ISNULL(P.MinBalance,0)            
               FROM t_AccountBalance ab            
               INNER JOIN t_Products P ON ab.OurBranchID = P.OurBranchID AND ab.ProductID = P.ProductID            
               Where ab.OurBranchID = @OurBranchID and ab.AccountID = @AccountID            
            
               select @DrawableBalance = @ClearBalance + @Limit - @FreezeAmount - @ProductMinBalance            
                           
    create table #tmpCharity (            
  [Status] Int,            
  Supervision varchar(10)            
    )            
            
    if @DrawableBalance >= @chAmount and @CurrencyID = @bCurrencyID            
    begin            
                 
   select @MaxScrollNo = isnull(max(ScrollNo),0) + 1 from t_TransferTransactionModel where OurBranchID=@OurBranchID            
             
   insert into #tmpCharity            
   exec spc_AddEditTransferTransaction            
   @ScrollNo = @MaxScrollNo,             
   @SerialID = 1,            
   @OurBranchID = @OurBranchID,            
   @AccountID = @AccountID,             
   @AccountName = @AccountName,             
   @ProductID = @ProductID,             
   @CurrencyID = @CurrencyID,             
   @AccountType = 'C',            
   @wDate = @wDate,             
   @TrxType = 'D',             
   @Amount = @chAmount,            
   @ForeignAmount = 0,            
   @ExchangeRate = 1,            
   @DescriptionID = '200190',            
   @Description = 'Charity donation',            
   @BankCode = @BankID,             
   @BranchCode = @OurBranchID,             
   @Supervision = 'N',             
   @IsSupervision = '0',             
   @OperatorID = 'COMPUTER',             
   @SupervisorID = 'N/A',             
   @PostingLimit = '999999999999',            
   @IsLocalCurrency = 1,            
   @TrxTimeStamp = @TrxTimeStamp          
             
   insert into #tmpCharity            
   exec spc_AddEditTransferTransaction            
   @ScrollNo = @MaxScrollNo,             
   @SerialID = 2,            
   @OurBranchID = @OurBranchID,            
   @AccountID = @CharityGL,             
   @AccountName = '',             
   @ProductID = 'GL',             
   @CurrencyID = @CurrencyID,             
  @AccountType = 'G',            
   @wDate = @wDate,             
   @TrxType = 'C',             
   @Amount = @chAmount,            
   @ForeignAmount = 0,            
   @ExchangeRate = 1,            
   @DescriptionID = '100001',            
   @Description = 'GL Credit against Charity donation',            
   @BankCode = @BankID,             
   @BranchCode = @OurBranchID,             
   @Supervision = 'N',         
   @IsSupervision = '0',             
   @OperatorID = 'COMPUTER',             
   @SupervisorID = 'N/A',             
   @PostingLimit = '999999999999',            
   @IsLocalCurrency = 1,            
   @TrxTimeStamp = @TrxTimeStamp            
             
    end            
            
  end            
            
  end            
            
   end            
            
   end            
   end            
            
   --Salary             
                 
   select @OurBranchID=OurBranchID, @AccountID=AccountID, @TrType=TrxType, @wDate=wDate, @AccountName=AccountName,                
   @ProductID=ProductID, @CurrencyID=CurrencyID, @Amount=Amount, @ForeignAmount=ForeignAmount,                
   @ExchangeRate=ExchangeRate, @DescriptionID = DescriptionID, @Description = Description, @TrxTimeStamp = TrxTimeStamp                
   from inserted                
                
   if exists(select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and ISNULL(IsSalary,0) = 1)                
   begin              
              
   insert into t_CustomerSalaries (OurBranchID,AccountID,TrxType,wDate,AccountName,ProductID,CurrencyID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,TrxTimeStamp)              
   values (@OurBranchID,@AccountID,@TrType,@wDate,@AccountName,@ProductID,@CurrencyID,@Amount,@ForeignAmount,@ExchangeRate,@DescriptionID,@Description,@TrxTimeStamp)              
              
   end                
                     
END   
GO
ALTER TABLE [dbo].[t_Transactions] ADD CONSTRAINT [PK_t_Transactions] PRIMARY KEY CLUSTERED ([OurBranchID], [ScrollNo], [SerialNo], [wDate], [DocumentType])
GO
