CREATE TABLE [dbo].[temp_DailyTaxCalc]
(
[AppWHTax] [bit] NOT NULL,
[ChargeRate] [money] NOT NULL,
[ChargesAmount] [money] NOT NULL,
[GLSerialID] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
