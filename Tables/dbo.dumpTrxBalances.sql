CREATE TABLE [dbo].[dumpTrxBalances]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearBalance] [money] NULL,
[TrxBalance] [money] NULL
)
GO
