CREATE TABLE [dbo].[t_UploadGLsCsv]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ReferenceNo] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Region] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BranchName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GLName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GLDate] [datetime] NOT NULL,
[GLType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GLSubType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GLSubTypeDesc] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Balance] [decimal] (21, 3) NOT NULL,
[GLSAPCode] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLSAPName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SAPBalance] [decimal] (21, 3) NULL
)
GO
ALTER TABLE [dbo].[t_UploadGLsCsv] ADD CONSTRAINT [PK_t_UploadGLsCsv] PRIMARY KEY CLUSTERED ([SerialNo])
GO
