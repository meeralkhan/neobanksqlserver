CREATE TABLE [dbo].[t_LinkFacilityAccount]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FacilityID] [decimal] (24, 0) NOT NULL,
[SerialID] [decimal] (24, 0) NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Limit] [money] NULL,
[EffectiveDate] [datetime] NULL,
[Limit1] [money] NULL,
[Rate1] [decimal] (18, 6) NULL,
[Limit2] [money] NULL,
[Rate2] [decimal] (18, 6) NULL,
[Limit3] [money] NULL,
[Rate3] [decimal] (18, 6) NULL,
[Limit4] [money] NULL,
[Rate4] [decimal] (18, 6) NULL,
[Limit5] [money] NULL,
[Rate5] [decimal] (18, 6) NULL,
[ExcessRate] [decimal] (18, 6) NULL,
[AccountLimitID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateType1] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateID1] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Margin1] [decimal] (18, 6) NULL,
[RateType2] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateID2] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateType3] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateID3] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Margin2] [decimal] (18, 6) NULL,
[Margin3] [decimal] (18, 6) NULL,
[ExcessRateType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcessRateID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcessMargin] [decimal] (18, 6) NULL,
[MinRate1] [decimal] (18, 6) NULL,
[MinRate2] [decimal] (18, 6) NULL,
[MinRate3] [decimal] (18, 6) NULL,
[ExcessMinRate] [decimal] (18, 6) NULL,
[NextChangeDate] [datetime] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create TRIGGER [dbo].[tr_LinkFacilityAccount_Add] ON [dbo].[t_LinkFacilityAccount]
AFTER INSERT
AS
BEGIN
   
   SET NOCOUNT ON

   declare @OurBranchID nvarchar(30)
   declare @ClientID nvarchar(30)
   declare @FacilityID decimal(24,0)
   declare @SerialID decimal(24,0)

   declare @AccountID nvarchar(30)
   declare @AccountLimitID nvarchar(30)

   declare @Limit money
   declare @EffectiveDate datetime

   declare @Limit1 money
   declare @Rate1 decimal(18,6)
   declare @MinRate1 decimal(18,6)

   declare @Limit2 money
   declare @Rate2 decimal(18,6)
   declare @MinRate2 decimal(18,6)
   
   declare @Limit3 money
   declare @Rate3 decimal(18,6)
   declare @MinRate3 decimal(18,6)
   
   declare @ExcessRate decimal(18,6)
   declare @ExcessMinRate decimal(18,6)
   declare @NextChangeDate datetime

   select @OurBranchID=OurBranchID, @ClientID=ClientID, @FacilityID=FacilityID, @SerialID=SerialID, @AccountID=AccountID, @AccountLimitID = AccountLimitID,
   @Limit=ISNULL(Limit,0), @Limit1=ISNULL(Limit1,0), @Limit2=ISNULL(Limit2,0), @Limit3=ISNULL(Limit3,0),
   @Rate1=ISNULL(Rate1,0), @Rate2=ISNULL(Rate2,0), @Rate3=ISNULL(Rate3,0), @ExcessRate=ISNULL(ExcessRate,0),
   @MinRate1=ISNULL(MinRate1,0), @MinRate2=ISNULL(MinRate2,0), @MinRate3=ISNULL(MinRate3,0), @ExcessMinRate=ISNULL(ExcessMinRate,0), @NextChangeDate = NextChangeDate
   from inserted

   select @EffectiveDate = WorkingDate from t_Last where OurBranchID = @OurBranchID

   delete from t_OverdraftLimits where OurBranchID = @OurBranchID and AccountID = @AccountID and EffectiveDate = @EffectiveDate
   
   if @ExcessRate = 0
      set @ExcessRate = @Rate1+2
	  
   if @ExcessMinRate = 0
      set @ExcessMinRate = @MinRate1+2

   insert into t_OverdraftLimits (OurBranchID, ClientID, FacilityID, SerialID, AccountID, AccountLimitID, Limit, EffectiveDate,
   Limit1, Rate1, MinRate1, Limit2, Rate2, MinRate2, Limit3, Rate3, MinRate3, ExcessRate, ExcessMinRate, LogDateTime,NextChangeDate)
   VALUES (@OurBranchID, @ClientID, @FacilityID, @SerialID, @AccountID, @AccountLimitID, @Limit, @EffectiveDate,
   @Limit1, @Rate1, @MinRate1, @Limit2, @Rate2, @MinRate2, @Limit3, @Rate3, @MinRate3, @ExcessRate, @ExcessMinRate, GETDATE(),@NextChangeDate)
   
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[tr_LinkFacilityAccount_Delete] ON [dbo].[t_LinkFacilityAccount]
AFTER DELETE
AS
BEGIN
   
   SET NOCOUNT ON

   declare @OurBranchID nvarchar(30)
   declare @ClientID nvarchar(30)
   declare @FacilityID decimal(24,0)
   declare @SerialID decimal(24,0)

   declare @AccountID nvarchar(30)
   declare @AccountLimitID nvarchar(30)

   declare @Limit money
   declare @EffectiveDate datetime

   declare @Limit1 money
   declare @Rate1 decimal(18,6)
   declare @MinRate1 decimal(18,6)

   declare @Limit2 money
   declare @Rate2 decimal(18,6)
   declare @MinRate2 decimal(18,6)
   
   declare @Limit3 money
   declare @Rate3 decimal(18,6)
   declare @MinRate3 decimal(18,6)
   
   declare @ExcessRate decimal(18,6)
   declare @ExcessMinRate decimal(18,6)

   select @OurBranchID=OurBranchID, @ClientID=ClientID, @FacilityID=FacilityID, @SerialID=SerialID, @AccountID=AccountID, @AccountLimitID = AccountLimitID,
   @Limit=ISNULL(Limit,0), @Limit1=ISNULL(Limit1,0), @Limit2=ISNULL(Limit2,0), @Limit3=ISNULL(Limit3,0),
   @Rate1=ISNULL(Rate1,0), @Rate2=ISNULL(Rate2,0), @Rate3=ISNULL(Rate3,0), @ExcessRate=ISNULL(ExcessRate,0),
   @MinRate1=ISNULL(MinRate1,0), @MinRate2=ISNULL(MinRate2,0), @MinRate3=ISNULL(MinRate3,0), @ExcessMinRate=ISNULL(ExcessMinRate,0)
   from deleted

   select @EffectiveDate = WorkingDate from t_Last where OurBranchID = @OurBranchID

   delete from t_OverdraftLimits where OurBranchID = @OurBranchID and AccountID = @AccountID and EffectiveDate = @EffectiveDate

   insert into t_OverdraftLimits (OurBranchID, ClientID, FacilityID, SerialID, AccountID, AccountLimitID, Limit, EffectiveDate,
   Limit1, Rate1, MinRate1, Limit2, Rate2, MinRate2, Limit3, Rate3, MinRate3, ExcessRate, ExcessMinRate, LogDateTime)
   VALUES (@OurBranchID, @ClientID, @FacilityID, @SerialID, @AccountID, @AccountLimitID, 0, @EffectiveDate,
   0, 0, 0, 0, 0, 0, 0, 0, 0, @ExcessRate, @ExcessMinRate, GETDATE())
   
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create TRIGGER [dbo].[tr_LinkFacilityAccount_Edit] ON [dbo].[t_LinkFacilityAccount]
AFTER UPDATE
AS
BEGIN
   
   SET NOCOUNT ON

   declare @OurBranchID nvarchar(30)
   declare @ClientID nvarchar(30)
   declare @FacilityID decimal(24,0)
   declare @SerialID decimal(24,0)

   declare @AccountID nvarchar(30)
   declare @AccountLimitID nvarchar(30)

   declare @Limit money
   declare @EffectiveDate datetime

   declare @Limit1 money
   declare @Rate1 decimal(18,6)
   declare @MinRate1 decimal(18,6)

   declare @Limit2 money
   declare @Rate2 decimal(18,6)
   declare @MinRate2 decimal(18,6)
   
   declare @Limit3 money
   declare @Rate3 decimal(18,6)
   declare @MinRate3 decimal(18,6)
   
   declare @ExcessRate decimal(18,6)
   declare @ExcessMinRate decimal(18,6)
   declare @NextChangeDate datetime

   select @OurBranchID=OurBranchID, @ClientID=ClientID, @FacilityID=FacilityID, @SerialID=SerialID, @AccountID=AccountID, @AccountLimitID = AccountLimitID,
   @Limit=ISNULL(Limit,0), @Limit1=ISNULL(Limit1,0), @Limit2=ISNULL(Limit2,0), @Limit3=ISNULL(Limit3,0),
   @Rate1=ISNULL(Rate1,0), @Rate2=ISNULL(Rate2,0), @Rate3=ISNULL(Rate3,0), @ExcessRate=ISNULL(ExcessRate,0),
   @MinRate1=ISNULL(MinRate1,0), @MinRate2=ISNULL(MinRate2,0), @MinRate3=ISNULL(MinRate3,0), @ExcessMinRate=ISNULL(ExcessMinRate,0),@NextChangeDate = NextChangeDate
   from inserted

   select @EffectiveDate = WorkingDate from t_Last where OurBranchID = @OurBranchID

   delete from t_OverdraftLimits where OurBranchID = @OurBranchID and AccountID = @AccountID and EffectiveDate = @EffectiveDate

   if @ExcessRate = 0
      set @ExcessRate = @Rate1+2
	  
   if @ExcessMinRate = 0
      set @ExcessMinRate = @MinRate1+2

   insert into t_OverdraftLimits (OurBranchID, ClientID, FacilityID, SerialID, AccountID, AccountLimitID, Limit, EffectiveDate,
   Limit1, Rate1, MinRate1, Limit2, Rate2, MinRate2, Limit3, Rate3, MinRate3, ExcessRate, ExcessMinRate, LogDateTime,NextChangeDate)
   VALUES (@OurBranchID, @ClientID, @FacilityID, @SerialID, @AccountID, @AccountLimitID, @Limit, @EffectiveDate,
   @Limit1, @Rate1, @MinRate1, @Limit2, @Rate2, @MinRate2, @Limit3, @Rate3, @MinRate3, @ExcessRate, @ExcessMinRate, GETDATE(),@NextChangeDate)
   
END
GO
ALTER TABLE [dbo].[t_LinkFacilityAccount] ADD CONSTRAINT [PK_t_LinkFacilityAccount] PRIMARY KEY CLUSTERED ([OurBranchID], [ClientID], [FacilityID], [SerialID])
GO
