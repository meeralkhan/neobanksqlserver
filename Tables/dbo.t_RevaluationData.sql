CREATE TABLE [dbo].[t_RevaluationData]
(
[CreateBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[LocEq] [money] NOT NULL,
[ExRate] [money] NOT NULL,
[RevAmount] [money] NOT NULL,
[PnL] [money] NOT NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GLControl] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
