CREATE TABLE [dbo].[Quantum_BRF_Deals]
(
[DEAL_NO] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEAL_DT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MATURE_DT] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPARTYNAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FACE_VALUE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REMAIN_FV] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MKT_VALUE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CCY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BASE_RATE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INT_RATE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RATING] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RATINGTYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONTRYCODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVESTMENTCATEGORY] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CPTYPECODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECTYPENAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SECTYPECODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BOR_INVEST] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MM_OR_SEC] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SWIFT_FI] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INVTYPE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLASS] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENTID] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APSTUS1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APSTUS2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IN_USE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCCODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCNAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[ToolRunDate] [datetime] NULL
)
GO
