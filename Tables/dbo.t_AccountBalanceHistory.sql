CREATE TABLE [dbo].[t_AccountBalanceHistory]
(
[wDate] [datetime] NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearBalance] [money] NOT NULL,
[LocalClearBalance] [money] NOT NULL,
[Effects] [money] NOT NULL,
[LocalEffects] [money] NOT NULL,
[Limit] [money] NOT NULL,
[OverdraftClassID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NoOfDPD] [decimal] (24, 0) NOT NULL
)
GO
ALTER TABLE [dbo].[t_AccountBalanceHistory] ADD CONSTRAINT [PK_t_AccountBalanceHistory] PRIMARY KEY CLUSTERED ([wDate], [OurBranchID], [AccountID])
GO
