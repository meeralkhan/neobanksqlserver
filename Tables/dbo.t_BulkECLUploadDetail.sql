CREATE TABLE [dbo].[t_BulkECLUploadDetail]
(
[CreateBy] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyBy] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[frmName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UploadSerialID] [decimal] (24, 0) NOT NULL,
[SerialNo] [decimal] (24, 0) NOT NULL,
[BranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [decimal] (13, 0) NULL,
[ECLStage] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ECLAmount] [money] NULL,
[SpecificProv] [money] NULL,
[GeneralProv] [money] NULL
)
GO
ALTER TABLE [dbo].[t_BulkECLUploadDetail] ADD CONSTRAINT [PK_t_BulkECLUploadDetail] PRIMARY KEY CLUSTERED ([OurBranchID], [UploadSerialID], [SerialNo])
GO
