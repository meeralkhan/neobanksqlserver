CREATE TABLE [dbo].[t_CriticalLog]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Username] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateTime] [datetime] NOT NULL,
[Terminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Module] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Severity] [int] NOT NULL,
[Message] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_CriticalLog] ADD CONSTRAINT [PK_t_CriticalLog] PRIMARY KEY CLUSTERED ([OurBranchID], [SerialNo])
GO
