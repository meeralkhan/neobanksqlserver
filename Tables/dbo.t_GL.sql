CREATE TABLE [dbo].[t_GL]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrintID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CategoryID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountClass] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPosting] [int] NULL,
[IsContigent] [int] NULL,
[OpeningBalance] [money] NULL,
[Balance] [money] NULL,
[ForeignOpeningBalance] [money] NULL,
[ForeignBalance] [money] NULL,
[TemporaryBalance] [money] NULL,
[Thru] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShadowBalance] [money] NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AvgBalProcessing] [int] NULL,
[RevaluationProcess] [int] NULL,
[IsReconcile] [int] NULL,
[GLOwnerID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLFSRollUps] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLSubGroups] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsMetaDataIncluded] [int] NULL,
[isExpenseMgmt] [int] NULL,
[CostTypesSPOID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create Trigger [dbo].[tr_InsertUpdateGLAccount]  on [dbo].[t_GL]
    AFTER INSERT, UPDATE
    as
    Begin
    declare @AccountID nvarchar(50)= ''
    declare @OurBranchID nvarchar(50)= ''
    select @AccountID = AccountID,@OurBranchID = OurBranchID from inserted
    IF NOT UPDATE(OpeningBalance) AND NOT UPDATE(Balance) AND NOT UPDATE(ForeignBalance) AND NOT UPDATE(ForeignOpeningBalance) AND NOT UPDATE(TemporaryBalance) AND NOT UPDATE(ShadowBalance)
    BEGIN
        insert into t_gl_Logs ([CreateBy],[CreateTime],[CreateTerminal],[UpdateBy],[UpdateTime],[UpdateTerminal],[AuthStatus],[SuperviseBy],[SuperviseTime],
        [SuperviseTerminal],[OurBranchID],[AccountID],[PrintID],[CategoryID],[Description],[AccountClass],[AccountType],[CurrencyID],[IsPosting],[IsContigent],
        [OpeningBalance],[Balance],[ForeignOpeningBalance],[ForeignBalance],[TemporaryBalance],[Thru],[Notes],[frmName],[ShadowBalance],[VerifyBy],
        [VerifyTime],[VerifyTerminal],[VerifyStatus],[AvgBalProcessing],[RevaluationProcess],[IsReconcile],GLOwnerID,GLFSRollUps,GLSubGroups,IsMetaDataIncluded,isExpenseMgmt) Select CreateBy,CreateTime,CreateTerminal,UpdateBy,
        UpdateTime,UpdateTerminal,AuthStatus,SuperviseBy,SuperviseTime,SuperviseTerminal,OurBranchID,AccountID,PrintID,CategoryID,Description,AccountClass,
        AccountType,CurrencyID,IsPosting,IsContigent,OpeningBalance,Balance,ForeignOpeningBalance,ForeignBalance,TemporaryBalance,Thru,Notes,frmName,ShadowBalance,
        VerifyBy,VerifyTime,VerifyTerminal,VerifyStatus,AvgBalProcessing,RevaluationProcess,IsReconcile,GLOwnerID,GLFSRollUps,GLSubGroups,IsMetaDataIncluded,isExpenseMgmt From t_GL where AccountID = @AccountID and OurBranchID = @OurBranchID
    END
END
GO
ALTER TABLE [dbo].[t_GL] ADD CONSTRAINT [PK_t_GL] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID])
GO
