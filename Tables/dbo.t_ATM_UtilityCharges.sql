CREATE TABLE [dbo].[t_ATM_UtilityCharges]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CompanyNumber] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WithdrawalCharges] [money] NOT NULL,
[WithdrawlChargesPercentage] [money] NOT NULL,
[IssuerAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IssuerAccountTitle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AcquiringAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcquiringAccountTitle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IssuerChargesAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IssuerTitle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcquiringChargesAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcquiringTitle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExciseDutyAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExciseDutyPercentage] [money] NULL,
[ChargeType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[t_ATM_UtilityCharges] ADD CONSTRAINT [PK_t_ATM_UtilityCharges] PRIMARY KEY CLUSTERED ([OurBranchID], [CompanyNumber])
GO
