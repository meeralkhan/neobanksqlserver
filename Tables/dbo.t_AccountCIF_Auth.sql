CREATE TABLE [dbo].[t_AccountCIF_Auth]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_AccountCIF_Auth] ADD CONSTRAINT [PK_t_AccountCIF_Auth] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [ClientID])
GO
