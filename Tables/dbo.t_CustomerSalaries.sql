CREATE TABLE [dbo].[t_CustomerSalaries]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1),
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxType] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[AccountName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [decimal] (21, 3) NOT NULL,
[ForeignAmount] [decimal] (21, 3) NOT NULL,
[ExchangeRate] [decimal] (18, 6) NOT NULL,
[DescriptionID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxTimeStamp] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[t_CustomerSalaries] ADD CONSTRAINT [PK_t_CustomerSalaries] PRIMARY KEY CLUSTERED ([SerialNo])
GO
