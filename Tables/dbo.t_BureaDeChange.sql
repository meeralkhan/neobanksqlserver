CREATE TABLE [dbo].[t_BureaDeChange]
(
[ScrollNo] [decimal] (18, 0) NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SellOrBuy] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ChequeID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Amount] [money] NOT NULL,
[ForeignAmount] [money] NOT NULL,
[ExchangeRate] [decimal] (18, 6) NOT NULL,
[Commission] [money] NOT NULL,
[CommRate] [decimal] (18, 6) NOT NULL,
[DescriptionID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NetAmount] [money] NOT NULL,
[ProfitOrLoss] [money] NOT NULL,
[CommAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CashAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProfitOrLossAccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supervision] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AdditionalData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_BureaDeChange] ADD CONSTRAINT [PK_t_BureaDeChange] PRIMARY KEY CLUSTERED ([ScrollNo])
GO
