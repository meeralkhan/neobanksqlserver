CREATE TABLE [dbo].[t_ATM_TransferTransaction]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ScrollNo] [int] NOT NULL,
[SerialNo] [numeric] (10, 0) NOT NULL,
[Refno] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WithdrawlBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ATMID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsLocalCurrency] [bit] NOT NULL,
[ValueDate] [datetime] NOT NULL,
[wDate] [datetime] NOT NULL,
[PHXDate] [datetime] NOT NULL,
[TrxType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Supervision] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GLID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [numeric] (18, 6) NOT NULL,
[ForeignAmount] [numeric] (18, 6) NULL,
[ExchangeRate] [decimal] (16, 8) NULL,
[DescriptionID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Remarks] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperatorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SupervisorID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MerchantType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreditCardNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CompanyNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STAN] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConvRate] [decimal] (15, 9) NULL,
[AcqCountryCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrCodeTran] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrCodeSett] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SettlmntAmount] [numeric] (18, 6) NULL,
[ATMStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSEntryMode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSConditionCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSPINCaptCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcqInstID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RetRefNum] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthIDResp] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardAccptID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CardAccptNameLoc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VISATrxID] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxDesc] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MCC] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CAVV] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForwardInstID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearingRefNo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create   TRIGGER [dbo].[tr_ATM_TransferTransaction_Insert] ON [dbo].[t_ATM_TransferTransaction]     
FOR INSERT    
AS     
     
 DECLARE @OurBranchID varchar(30)    
 DECLARE @AccountID varchar(30)     
 DECLARE @ProductID varchar(30)    
 DECLARE @Amount money    
 
 DECLARE @STAN as varchar(30)
 DECLARE @PHXDate as datetime 
 DECLARE @wDate as datetime 
 DECLARE @ForeignAmount as money
 DECLARE @ExchangeRate as decimal(16,8)
 DECLARE @MerchantType as varchar(30)
 DECLARE @AcqCountryCode as varchar(3)
 DECLARE @ATMID as varchar(30)
 DECLARE @CurrCodeTran as varchar(3)
 DECLARE @CurrCodeSett as varchar(3)
 DECLARE @SettlmntAmount as money
 DECLARE @ProcCode as varchar(50)
 DECLARE @POSEntryMode as varchar(3)
 DECLARE @POSConditionCode as varchar(2)
 DECLARE @POSPINCaptCode as varchar(2)
 DECLARE @AcqInstID as varchar(30)  
 DECLARE @RetRefNum as varchar(15)
 DECLARE @CardAccptID as varchar(15)
 DECLARE @CardAccptNameLoc as varchar(50)
 DECLARE @Description       as varchar (255)   
 DECLARE @CAVV       as char (1)   
 DECLARE @DescriptionID as varchar(30)
 DECLARE @RefNo as varchar(100)
 DECLARE @ConvRate as decimal(15,9)
 DECLARE @ForwardInstID as varchar(10)
 DECLARE @VISATrxID as varchar(15)
 DECLARE @ResponseCode as varchar(2)
 DECLARE @AuthIDResp as varchar(6)
  SET @AuthIDResp = '0'
 
 DECLARE @NoOfCWTrxATMMonthly as decimal(24,0)
 DECLARE @NoOfBITrxATMMonthly as decimal(24,0)
 DECLARE @NoOfFreeCW decimal(24,0)
  set @NoOfFreeCW=0
  
 DECLARE @ClearBalance as money
 DECLARE @Limit as money
 DECLARE @FreezeAmount as money
 DECLARE @MinBalance as money
    
 IF (SELECT AccountType FROM Inserted)='C'     
 BEGIN       
  SELECT @OurBranchID=OurBranchID,@AccountID=AccountID,@ProductID=ProductID,@RefNo=RefNo,@STAN=STAN,@PHXDate=PHXDate,@wDate=wDate,@ForeignAmount=ForeignAmount,
  @ExchangeRate=ExchangeRate,@ConvRate=ConvRate,@Amount=Amount,@MerchantType=MerchantType,@AcqCountryCode=AcqCountryCode,@ForwardInstID=ForwardInstID,@ATMID=ATMID,
  @CurrCodeTran=CurrCodeTran,@CurrCodeSett=CurrCodeSett,@SettlmntAmount=SettlmntAmount,@ProcCode=ProcCode,@POSEntryMode=POSEntryMode,@POSConditionCode=POSConditionCode,
  @POSPINCaptCode=POSPINCaptCode,@AcqInstID=AcqInstID,@RetRefNum=RetRefNum,@AuthIDResp=AuthIDResp,@CardAccptID=CardAccptID,@CardAccptNameLoc=CardAccptNameLoc,
  @Description=ISNULL([Description],''),@CAVV=CAVV,@DescriptionID=DescriptionID
  FROM Inserted    
  
  IF (SELECT Supervision FROM Inserted)='C'     
   IF (SELECT TrxType FROM Inserted)='D'     
   BEGIN    
    UPDATE t_AccountBalance SET ClearBalance=(ClearBalance - @Amount)
	WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID    
   END    
   IF (SELECT TrxType FROM Inserted)='C'     
   BEGIN    
    UPDATE t_AccountBalance SET ClearBalance=(ClearBalance + @Amount)
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID    
   END    
   
   SELECT @ClearBalance=ISNULL(B.ClearBalance,0),@FreezeAmount=ISNULL(B.FreezeAmount,0),@MinBalance=ISNULL(P.MinBalance,0),@Limit=ISNULL(B.Limit,0)
   FROM t_AccountBalance B           
   INNER JOIN t_Products P ON B.OurBranchID = P.OurBranchID AND B.ProductID = P.ProductID
   WHERE (B.OurBranchID = @OurBranchID) and (B.AccountID = @AccountID)
   
   IF (@Amount > (@ClearBalance+@Limit-@FreezeAmount-@MinBalance))
   BEGIN
    INSERT INTO t_ForceDebitHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
    ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, AvailableBalance, AuthIDResp)
    VALUES ( @OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @wDate, @Amount, @ForeignAmount, @ExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID,
    @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AcqInstID, @RetRefNum, @CardAccptID, 
    @CardAccptNameLoc, @Description, @VISATrxID, @CAVV, @ResponseCode, @DescriptionID, (@ClearBalance+@Limit-@FreezeAmount-@MinBalance), @AuthIDResp )
   END
 END    
 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



create   TRIGGER [dbo].[tr_ATM_TransferTransaction_Update] ON [dbo].[t_ATM_TransferTransaction]     
FOR UPDATE    
AS     
     
 DECLARE @OurBranchID varchar(30)    
 DECLARE @AccountID varchar(30)    
 DECLARE @Amount money    
    
 IF (SELECT AccountType FROM Inserted)='C'     
 BEGIN       
  SELECT @AccountID=AccountID, @OurBranchID=OurBranchID,@Amount=Amount   FROM Inserted    
    
  IF (SELECT Supervision FROM Inserted)='R'     
   IF (SELECT TrxType FROM Inserted)='D'     
   BEGIN    
    UPDATE t_AccountBalance SET ClearBalance=(ClearBalance + @Amount)
	WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID    
   END
   if (SELECT TrxType FROM Inserted)='C'
   BEGIN
    UPDATE t_AccountBalance SET ClearBalance=(ClearBalance - @Amount)
	WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID    
   END
 END    
 
 
GO
ALTER TABLE [dbo].[t_ATM_TransferTransaction] ADD CONSTRAINT [PK_t_ATM_TransferTransaction] PRIMARY KEY CLUSTERED ([SerialNo], [Refno])
GO
