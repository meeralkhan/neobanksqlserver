CREATE TABLE [dbo].[t_ATM_Temp]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ValueDate] [datetime] NOT NULL,
[wDate] [datetime] NOT NULL,
[Amount] [money] NOT NULL,
[TrxType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TrxDesc] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DocumentType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
