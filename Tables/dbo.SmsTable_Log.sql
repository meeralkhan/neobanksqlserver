CREATE TABLE [dbo].[SmsTable_Log]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Exception] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL,
[ChannelRefId] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmsId] [int] NULL
)
GO
ALTER TABLE [dbo].[SmsTable_Log] ADD CONSTRAINT [PK_SmsTable_Log] PRIMARY KEY CLUSTERED ([Id])
GO
