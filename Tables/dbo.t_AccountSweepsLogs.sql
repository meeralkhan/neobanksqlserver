CREATE TABLE [dbo].[t_AccountSweepsLogs]
(
[SerialNo] [decimal] (24, 0) NOT NULL IDENTITY(1, 1),
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SweepType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExceedAmount] [decimal] (21, 3) NOT NULL,
[AccessFundsAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[wDate] [datetime] NOT NULL,
[DateTime] [datetime] NOT NULL,
[Status] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[t_AccountSweepsLogs] ADD CONSTRAINT [PK_t_AccountSweepsLogs] PRIMARY KEY CLUSTERED ([SerialNo])
GO
