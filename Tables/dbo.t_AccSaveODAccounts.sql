CREATE TABLE [dbo].[t_AccSaveODAccounts]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MeanRate] [decimal] (18, 6) NULL,
[CcyRounding] [decimal] (24, 0) NULL,
[IBAN] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpenDate] [datetime] NULL,
[Accrual] [money] NULL,
[AccrualUpto] [datetime] NULL,
[PAccrual] [money] NULL,
[PAccrualUpto] [datetime] NULL,
[IsProcessed] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[t_AccSaveODAccounts] ADD CONSTRAINT [PK_t_AccSaveODAccounts] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID])
GO
