CREATE TABLE [dbo].[t_Provisioning]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialID] [int] NOT NULL,
[ProvDate] [datetime] NOT NULL,
[AccountName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MeanRate] [decimal] (18, 6) NOT NULL,
[LastProvStage] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastProvAmount] [money] NOT NULL,
[LastProvDate] [datetime] NULL,
[NewProvStage] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewProvAmount] [money] NOT NULL,
[NewProvDate] [datetime] NOT NULL,
[FSVBenefit] [decimal] (18, 6) NOT NULL,
[Provisioning] [decimal] (18, 6) NOT NULL,
[Type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReverseAmount] [money] NOT NULL
)
GO
ALTER TABLE [dbo].[t_Provisioning] ADD CONSTRAINT [PK_t_Provisioning] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [DealID], [SerialID])
GO
