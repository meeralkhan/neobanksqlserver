CREATE TABLE [dbo].[t_Last]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LASTEOD] [datetime] NULL,
[LASTBOD] [datetime] NULL,
[EODEND] [datetime] NULL,
[CONFRETI] [datetime] NULL,
[CONFRETB] [datetime] NULL,
[CONFRETT] [datetime] NULL,
[LASTDAY] [datetime] NULL,
[LASTDAYBAK] [datetime] NULL,
[LASTEODBAK] [datetime] NULL,
[LSTAGEFFDT] [datetime] NULL,
[LSTODINTDT] [datetime] NULL,
[LASTINTDT] [datetime] NULL,
[INTDUEON] [datetime] NULL,
[STARTYEAR] [datetime] NULL,
[ENDYEAR] [datetime] NULL,
[EOMCALCDT] [datetime] NULL,
[EOMUPDATE] [datetime] NULL,
[WORKINGDATE] [datetime] NULL,
[frmName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
