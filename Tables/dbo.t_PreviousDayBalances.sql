CREATE TABLE [dbo].[t_PreviousDayBalances]
(
[WorkingDate] [datetime] NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClearBalance] [money] NOT NULL,
[Effects] [money] NOT NULL,
[LocalClearBalance] [money] NOT NULL,
[LocalEffects] [money] NOT NULL
)
GO
