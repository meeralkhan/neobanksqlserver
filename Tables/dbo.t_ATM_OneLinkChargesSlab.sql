CREATE TABLE [dbo].[t_ATM_OneLinkChargesSlab]
(
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SerialID] [int] NOT NULL,
[AmountFrom] [money] NOT NULL,
[AmountTo] [money] NOT NULL,
[Charges] [money] NOT NULL
)
GO
ALTER TABLE [dbo].[t_ATM_OneLinkChargesSlab] ADD CONSTRAINT [PK_t_ATM_OneLinkChargesSlab] PRIMARY KEY CLUSTERED ([OurBranchID], [SerialID])
GO
