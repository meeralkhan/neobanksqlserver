CREATE TABLE [dbo].[EmailTable_Log]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Exception] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL,
[ChannelRefId] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Level] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailId] [int] NULL
)
GO
ALTER TABLE [dbo].[EmailTable_Log] ADD CONSTRAINT [PK_EmailTable_Log] PRIMARY KEY CLUSTERED ([Id])
GO
