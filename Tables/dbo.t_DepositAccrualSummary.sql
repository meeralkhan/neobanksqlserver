CREATE TABLE [dbo].[t_DepositAccrualSummary]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReceiptID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[MaturityDate] [datetime] NOT NULL,
[FromDate] [datetime] NOT NULL,
[ToDate] [datetime] NOT NULL,
[Days] [int] NOT NULL,
[PrincipalAmount] [money] NOT NULL,
[Rate] [money] NOT NULL,
[PremiumRate] [money] NOT NULL,
[OpeningBalance] [money] NOT NULL,
[Amount] [money] NOT NULL,
[ProfitPaid] [money] NOT NULL,
[ClosingBalance] [money] NOT NULL,
[Period] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CurrentDays] [int] NOT NULL,
[CurrentRate] [money] NOT NULL,
[CurrentAmount] [money] NOT NULL,
[DiffAmount] [money] NOT NULL,
[WorkingDate] [datetime] NULL
)
GO
