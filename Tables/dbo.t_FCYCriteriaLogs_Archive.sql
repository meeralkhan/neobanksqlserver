CREATE TABLE [dbo].[t_FCYCriteriaLogs_Archive]
(
[ChannelRefID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogTime] [datetime] NULL,
[ProcessedTime] [datetime] NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GUID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[mGUID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FailedDescription] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CriteriaDetails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActualTrxDetails] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdditionalTrxRequest] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdditionalTrxResponse] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxPostedWithCriteria] [bit] NULL
)
GO
ALTER TABLE [dbo].[t_FCYCriteriaLogs_Archive] ADD CONSTRAINT [PK_t_FCYCriteriaLogs_Archive] PRIMARY KEY CLUSTERED ([OurBranchID], [GUID])
GO
