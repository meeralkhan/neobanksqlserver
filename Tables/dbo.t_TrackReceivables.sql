CREATE TABLE [dbo].[t_TrackReceivables]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HoldRefNo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsFreeze] [int] NOT NULL,
[wDate] [datetime] NOT NULL,
[Amount] [money] NOT NULL,
[Comments] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReleaseDate] [datetime] NULL,
[ReleaseComments] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InstNo] [decimal] (24, 0) NOT NULL
)
GO
ALTER TABLE [dbo].[t_TrackReceivables] ADD CONSTRAINT [PK_t_TrackReceivables] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [DealID], [HoldRefNo])
GO
