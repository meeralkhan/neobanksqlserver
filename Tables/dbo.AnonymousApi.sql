CREATE TABLE [dbo].[AnonymousApi]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ControllerName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActionName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AnonymousApi] ADD CONSTRAINT [PK_AnonymousApi] PRIMARY KEY CLUSTERED ([Id])
GO
