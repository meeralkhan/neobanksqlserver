CREATE TABLE [dbo].[t_UserAccessLogs]
(
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperatorID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WDateTime] [datetime] NOT NULL,
[SQLDateTime] [datetime] NOT NULL,
[ServerDateTime] [datetime] NOT NULL,
[AuthType] [bit] NOT NULL,
[frmName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Terminal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
