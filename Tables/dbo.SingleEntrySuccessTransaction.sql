CREATE TABLE [dbo].[SingleEntrySuccessTransaction]
(
[UniqueId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChannelId] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateTime] [datetime] NULL,
[ChannelRefId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransferEntryBranchId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EntryType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NarrationId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExchangeRate] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocalEq] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Memo] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL,
[TrxMemo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxNarrationId] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentChannelRefId] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestParamter] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResponseParameter] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SingleEntrySuccessTransaction] ADD CONSTRAINT [PK_SingleEntrySuccessTransaction] PRIMARY KEY CLUSTERED ([UniqueId])
GO
