CREATE TABLE [dbo].[t_PreviousDayBalances_Cust]
(
[CreateBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OurBranchID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WorkingDate] [datetime] NOT NULL,
[AccountID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProductID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClearBalance] [money] NULL,
[Effects] [money] NULL,
[LocalClearBalance] [money] NULL,
[LocalEffects] [money] NULL
)
GO
ALTER TABLE [dbo].[t_PreviousDayBalances_Cust] ADD CONSTRAINT [PK_t_PreviousDayBalances_Cust] PRIMARY KEY CLUSTERED ([OurBranchID], [WorkingDate], [AccountID])
GO
