CREATE TABLE [dbo].[t_Disbursement]
(
[CreateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateTime] [datetime] NOT NULL,
[CreateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [datetime] NULL,
[UpdateTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuperviseBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuperviseTime] [datetime] NULL,
[SuperviseTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[frmName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OurBranchID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealID] [decimal] (24, 0) NOT NULL,
[ReferenceNo] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbursementDate] [datetime] NULL,
[FirstPaymentOn] [datetime] NULL,
[AdvancesDate] [datetime] NULL,
[MaturityDate] [datetime] NULL,
[ProfitRateType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProfitRate] [money] NULL,
[Amount] [money] NULL,
[Method] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaymentMethod] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PoolID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Remarks] [nvarchar] (800) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxAccountType] [nvarchar] (320) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BaseRate] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BasePercent] [money] NULL,
[Margin] [money] NULL,
[ChargeID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrokerID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fee] [money] NULL,
[Charges] [money] NULL,
[TrxAccID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrxGLAccID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuthStatus2] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScrollNo] [decimal] (24, 0) NULL,
[CloseDate] [datetime] NULL,
[IsClosed] [int] NULL,
[Status] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StopDate] [datetime] NULL,
[StoppedBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StopReason] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyBy] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyTime] [datetime] NULL,
[VerifyTerminal] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GracePeriod] [decimal] (24, 0) NULL,
[Exempt] [decimal] (24, 0) NULL,
[ExemptType] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExemptPayType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssetValue] [money] NULL,
[CategoryID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RiskCode] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InstrumentType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DrawnBank] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DrawnBranch] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TTMode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InFavourOf] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemitCharges] [money] NULL,
[LocalOpeningBalance] [money] NULL,
[LocalClosingBalance] [money] NULL,
[ForeignOpeningBalance] [money] NULL,
[ForeignClosingBalance] [money] NULL,
[BalanceMeanRate] [money] NULL,
[Accrual] [money] NULL,
[tAccrual] [money] NOT NULL,
[PAccrual] [money] NOT NULL,
[tPAccrual] [money] NOT NULL,
[AccrualUpto] [datetime] NULL,
[FSVCollateralValue] [money] NULL,
[LiquidAssetValue] [money] NULL,
[ProvStage] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ProvAmount] [money] NOT NULL,
[LastProvDate] [datetime] NULL,
[LastPayDate] [datetime] NULL,
[IsPreMatureClosed] [int] NOT NULL,
[SplitRate] [money] NULL,
[FloorRate] [money] NULL,
[CapRate] [money] NULL,
[FacilityID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[minEsFee] [decimal] (21, 3) NULL,
[esFeePercentage] [decimal] (18, 6) NULL,
[feesAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[interestAccountID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[esFeeType] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tenure] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenureType] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvPenalRate] [decimal] (18, 6) NULL,
[FacilitySerialID] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OutstandingBalance] [money] NULL,
[EffectiveDate] [datetime] NULL,
[NextChangeDate] [datetime] NULL,
[ECLStage] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ECLAmount] [money] NULL,
[SpecificProvisioning] [money] NULL,
[GeneralProvisioning] [money] NULL,
[DailyPenalInt] [money] NULL,
[DisableAutoClassification] [int] NULL,
[NoOfDPD] [decimal] (24, 0) NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   TRIGGER [dbo].[tr_Disbursement_Add] ON [dbo].[t_Disbursement]
AFTER INSERT
AS
BEGIN
   
   SET NOCOUNT ON
   
   declare @OurBranchID nvarchar(30)
   declare @ClientID nvarchar(30)
   declare @FacilityID decimal(24,0)
   declare @AccountID nvarchar(30)
   declare @ProductID nvarchar(30)
   declare @CurrencyID nvarchar(30)
   declare @DealID decimal(24,0)
   declare @DealRefNo nvarchar(30)
   declare @Limit money
   declare @EffectiveDate datetime
   declare @ProfitRateType nvarchar(30)
   declare @ProfitRate decimal(18,6)
   declare @BaseRate nvarchar(30)
   declare @BasePercent decimal(18,6)
   declare @Margin decimal(18,6)
   declare @FloorRate decimal(18,6)
   declare @CapRate decimal(18,6)
   declare @Limit1 money
   declare @Rate1 decimal(18,6)
   declare @ExcessRate decimal(18,6)
   declare @NextChangeDate datetime
   
   select @OurBranchID=d.OurBranchID, @ClientID=a.ClientID, @FacilityID=FacilitySerialID, @AccountID=d.AccountID, @ProductID=a.ProductID, @CurrencyID=a.CurrencyID,
   @DealID=d.DealID, @DealRefNo=d.ReferenceNo, @Limit=Amount, @ProfitRateType=ProfitRateType, @ProfitRate=ProfitRate, @BaseRate=BaseRate, @BasePercent=BasePercent,
   @Margin=Margin, @FloorRate=FloorRate, @CapRate=CapRate, @Limit1=Amount, @Rate1=ProfitRate, @ExcessRate=AdvPenalRate, @NextChangeDate=NextChangeDate
   from inserted d
   inner join t_Account a on d.OurBranchID = a.OurBranchID and d.AccountID = a.AccountID

   select @EffectiveDate = WorkingDate from t_Last where OurBranchID = @OurBranchID
   
   delete from t_TermLoanLimits where OurBranchID = @OurBranchID and AccountID = @AccountID and DealID = @DealID and EffectiveDate = @EffectiveDate

   insert into t_TermLoanLimits (OurBranchID, ClientID, FacilityID, AccountID, ProductID, CurrencyID, DealID, DealRefNo, Limit, EffectiveDate, ProfitRateType,
   ProfitRate, BaseRate, BasePercent, Margin, FloorRate, CapRate, Limit1, Rate1, ExcessRate, LogDateTime, NextChangeDate)
   VALUES (@OurBranchID, @ClientID, @FacilityID, @AccountID, @ProductID, @CurrencyID, @DealID, @DealRefNo, @Limit, @EffectiveDate, @ProfitRateType, 
   @ProfitRate, @BaseRate, @BasePercent, @Margin, @FloorRate, @CapRate, @Limit1, @Rate1, @ExcessRate, GETDATE(),@NextChangeDate)
   
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   TRIGGER [dbo].[tr_Disbursement_Edit] ON [dbo].[t_Disbursement]
AFTER UPDATE
AS
BEGIN
   
   SET NOCOUNT ON
   
   IF UPDATE (ProfitRate)
   begin
      
	  declare @OurBranchID nvarchar(30)
      declare @ClientID nvarchar(30)
      declare @FacilityID decimal(24,0)
      declare @AccountID nvarchar(30)
      declare @ProductID nvarchar(30)
      declare @CurrencyID nvarchar(30)
      declare @DealID decimal(24,0)
      declare @DealRefNo nvarchar(30)
      declare @Limit money
      declare @EffectiveDate datetime
      declare @ProfitRateType nvarchar(30)
      declare @ProfitRate decimal(18,6)
      declare @BaseRate nvarchar(30)
      declare @BasePercent decimal(18,6)
      declare @Margin decimal(18,6)
      declare @FloorRate decimal(18,6)
      declare @CapRate decimal(18,6)
      declare @Limit1 money
      declare @Rate1 decimal(18,6)
      declare @ExcessRate decimal(18,6)
      declare @NextChangeDate datetime
      
	  select @OurBranchID=d.OurBranchID, @ClientID=a.ClientID, @FacilityID=FacilitySerialID, @AccountID=d.AccountID, @ProductID=a.ProductID, @CurrencyID=a.CurrencyID,
      @DealID=d.DealID, @DealRefNo=d.ReferenceNo, @Limit=Amount, @ProfitRateType=ProfitRateType, @ProfitRate=ProfitRate, @BaseRate=BaseRate, @BasePercent=BasePercent,
      @Margin=Margin, @FloorRate=FloorRate, @CapRate=CapRate, @Limit1=Amount, @Rate1=ProfitRate, @ExcessRate=AdvPenalRate, @NextChangeDate=NextChangeDate
      from inserted d
      inner join t_Account a on d.OurBranchID = a.OurBranchID and d.AccountID = a.AccountID
      
	  select @EffectiveDate = WorkingDate from t_Last where OurBranchID = @OurBranchID

	  declare @ReducedLimit money = 0
	  select @ReducedLimit = ISNULL(SUM(PrincipalAmount),0) from t_Adv_PaymentSchedule
	  where OurBranchID=@OurBranchID and AccountID=@AccountID and DealID=@DealID and PrincipalAmount > 0 and DueDate <= @EffectiveDate

	  set @Limit1 = @Limit1 - @ReducedLimit;
	  if @Limit1 < 0
	     set @Limit1 = 0

      delete from t_TermLoanLimits where OurBranchID = @OurBranchID and AccountID = @AccountID and DealID = @DealID and EffectiveDate = @EffectiveDate
      
      insert into t_TermLoanLimits (OurBranchID, ClientID, FacilityID, AccountID, ProductID, CurrencyID, DealID, DealRefNo, Limit, EffectiveDate, ProfitRateType,
      ProfitRate, BaseRate, BasePercent, Margin, FloorRate, CapRate, Limit1, Rate1, ExcessRate, LogDateTime, NextChangeDate)
      VALUES (@OurBranchID, @ClientID, @FacilityID, @AccountID, @ProductID, @CurrencyID, @DealID, @DealRefNo, @Limit, @EffectiveDate, @ProfitRateType, 
      @ProfitRate, @BaseRate, @BasePercent, @Margin, @FloorRate, @CapRate, @Limit1, @Rate1, @ExcessRate, GETDATE(),@NextChangeDate)

   end
END
GO
ALTER TABLE [dbo].[t_Disbursement] ADD CONSTRAINT [PK_t_Disbursement] PRIMARY KEY CLUSTERED ([OurBranchID], [AccountID], [DealID])
GO
