SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROCEDURE [dbo].[sp_GetGlobalVariables]  
 @OurBranchID varchar(30)
 AS  
 SELECT * FROM t_GlobalVariables  
 Where (OurBranchID = @OurBranchID)
GO
