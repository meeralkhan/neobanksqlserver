SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[spc_Exec_DumpQuery]        
(        
 @SQLQuery nvarchar(max)   
)        
AS        
        
SET NOCOUNT ON    
  
exec (@SQLQuery)      
select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage 
GO
