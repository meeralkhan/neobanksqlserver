SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_GetWorkingDate]
@OurBranchID varchar(30)
AS  
  
 BEGIN  
  SELECT lasteod,lastbod,workingdate FROM t_Last  
  WHERE OurBranchID = @OurBranchID
 END
GO
