SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE proc [dbo].[updateaccountfree]
@accountid as nvarchar(max),
@serial as numeric(18,2)
as
update t_AccountFreeze set IsFreeze = 0 where AccountID = @accountid and Serial = @serial
select * from t_AccountFreeze where AccountID = @accountid and Serial = @serial
GO
