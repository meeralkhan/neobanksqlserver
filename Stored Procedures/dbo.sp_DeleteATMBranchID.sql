SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_DeleteATMBranchID]
(    
  @OurBranchID varchar(30),
  @BranchID varchar(30)
 )    
AS    
     
   DELETE FROM t_ATM_Branches
   WHERE  OurBranchID = @OurBranchID AND BranchID = @BranchID
GO
